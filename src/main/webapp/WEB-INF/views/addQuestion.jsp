<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<script src="${pageContext.request.contextPath}/resources/ace.js" type="text/javascript" charset="utf-8"></script>    
<script src="${pageContext.request.contextPath}/resources/jquery.validate.js" type="text/javascript" charset="utf-8"></script>
</head>

<style>
.ErrorField {
	border-color: #D00;
	color: #D00;
	background: #FFFFFE;
}

span.ValidationErrors {
    display: block !important;
	min-height: 19px;
    font-size: 12px;
    color: #D00;
    padding-left: 10px;
    font-family: sans-serif;
}
</style>
<script>

$(function() {
	 	
	jQuery("#questionID").validate({
        expression: "if (VAL) return true; else return false;",
        message: "Please enter question."
    });
	jQuery("#qTimeID").validate({
        expression: "if (VAL) return true; else return false;",
       message: "Please enter time per question."
    });
	
	jQuery("#validCheckBox").validate({
        expression: "if (isChecked(SelfID)) return true; else return false;",
        message: "Please select a radio button"
    });
	
	jQuery("#fileNameID").validate({
        expression: "if (VAL) return true; else return false;",
        message: "Please enter file name."
    });
	
	jQuery("#testCodeID").validate({
        expression: "if (VAL) return true; else return false;",
        message: "Please enter Unit Test Case."
    });
	
	jQuery("#testFileNM").validate({
        expression: "if (VAL) return true; else return false;",
        message: "Please enter Test File Name."
    });
	
	jQuery("#testWtID").validate({
        expression: "if (VAL) return true; else return false;",
        message: "Please enter Test weightage."
    });
	
	
	
	var finalTestValues=[];
	
	$("#questionTypeID").selectmenu({
		change: function( event, ui ) {
			var editor = ace.edit("editor");
			if($("#questionTypeID").val() == "Programming" ){
				editor.setReadOnly(false);
				$("#testCaseSection textarea, #testCaseSection input, #testCaseSection select").attr('disabled',false);
			}else{
				editor.setValue('',0);
				editor.setReadOnly(true);
				$("#testCaseSection textarea, #testCaseSection input, #testCaseSection select").attr('disabled',true);
			}
		}
	});
	$("#questionCompID").selectmenu();
	$("#questionTechID").selectmenu();
	$("#questionActID").selectmenu();
	
    $( "#accordion" ).accordion({
      collapsible: true
    });
    
    $("#addTestCaseID")
	.click(function() {
		
		
			if($("#questionTypeID").val() == "Programming" && isNotNull($('#testCodeID').val()) && isNotNull($('#testFileNMID').val()) && isNotNull($('#testWtID').val())){
				$("#testCaseID")
				.append(
						'<li><label>Test Code <span class="required">*</span></label><textarea name="testCode" id="testCodeID" class="field-long field-textarea"></textarea></li><li> <input type="text" name="testFileNM"	id="testFileNMID" class="field-divided" placeholder="Test File Name" required="required"/> <input type="text" name="testWt"	id="testWtID" class="field-divided" placeholder="Test Weightage" required="required"/></li>');
			}else{
				alert("Please all the mandatory test case fields.")
			}
			
		
				
			});
    
	

});

function isNotNull(test){
	if(test !=null && test != ''){
		return true;
	}else{
		return false;
	}
}




$(document).ready(function(){
	
	$("#addQuestion").click(function(){

		if($("#questionTypeID").val() == "Programming" && isNotNull($('#testCodeID').val()) && isNotNull($('#testFileNMID').val()) && isNotNull($('#testWtID').val())){
			
    	}else{
			alert("Please all the mandatory test case fields.")
		}
    	
    	var carDataString = JSON.stringify($('#questionForm').serializeObject());
    	
    	alert(carDataString);
    	
         $.ajax({
            type: "POST",
            url: "/InterviewAssist/addQuestionSet",
            data:  carDataString,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data){
                 alert(data);//handle it in a proper way
            },
            failure: function(errMsg) {
               alert(errMsg);//handle it in a proper way
            }
        });
	});
	
	
});


</script>

<form action="#" id="questionForm">


<div id="accordion" style="margin-top:20px">
  <h3 style="margin-bottom: 2px;">Question</h3>
  <div>
    <ul class="form-style-1">
	<li>
        <label>Question <span class="required">*</span></label>
        <textarea name="question" id="questionID" class="field-long field-textarea" placeholder="Enter The Question"></textarea>
    </li>
    <li>
        <label>Technology <span class="required">*</span></label><label>Complexity <span class="required">*</span></label>
        <select name="questionTech" id="questionTechID">
	      <option selected="selected">Java</option>
	      <option>C</option>
	      <option>C++</option>
	      <option>C#</option>
	      <option>.net</option>
		</select> 
		 
       <select name="questionComp" id="questionCompID">
	      <option selected="selected">Simple</option>
	      <option>Medium</option>
	      <option>Complex</option>
		</select> 
    </li>
    
     <li>
        <label>Question Type <span class="required">*</span></label><label>Active <span class="required">*</span></label>
       <select name="questionType" id="questionTypeID">
	      <option>MCQ</option>
	      <option selected="selected">Programming</option>
		</select>
		 
      <select name="questionAct" id="questionActID">
	      <option selected="selected">Y</option>
	      <option>N</option>
	   </select> 
    </li>
    
    <li>
   	 <input type="text" name="qTime"	id="qTimeID" class="field-divided" placeholder="Time Per Question"/>
	</li>
	<li>
		<span id="validCheckBox" class="levels">
	 <label style="margin-right: 20px;">Applicability <span class="required">*</span></label>
	   	<input type="checkbox" id="validCheckBox_1" name="role" value="SE" placeholder="Software Engineeer">SE
	   	<input type="checkbox" id="validCheckBox_2"  name="role" value="SSE" placeholder="Senior Software Engineeer">SSE
	   	<input type="checkbox" id="validCheckBox_3" name="role" value="ML" placeholder="Module Lead">ML
	   	<input type="checkbox" id="validCheckBox_4" name="role" value="TL" placeholder="Technical Lead">TL
	   	<input type="checkbox" id="validCheckBox_5" name="role" value="PL" placeholder="Project Lead">PL
	   	<input type="checkbox" id="validCheckBox_6" name="role" value="PM" placeholder="Project Manager">PM
	   	</span>
	</li>
	</ul>
  </div>
  <h3>Pseudo Code</h3>
  <div>
    <p>
     <div id="editor"  style="overflow: scroll; position: relative; height:300px;width:700px;">
   </div>
   <div style="margin-left: 100px;">
   <ul class="form-style-1" >
   	  <li >
   	 <input type="text" name="fileName"	id="fileNameID" class="field-divided" placeholder="Enter File Name"/>
	</li>
   </ul></div>
    </p>
  </div>
  
   <h3>Test Case</h3>
  <div id="testCaseSection">
    <ul class="form-style-1" id="testCaseID">
	<li>
        <label>Test Code <span class="required">*</span></label>
        <textarea name="testCode" id="testCodeID" class="field-long field-textarea" placeholder="Unit Test Code"></textarea>
    </li>
   	
   	<li>
   	 <input type="text" name="testFileNM"	id="testFileNMID" class="field-divided" placeholder="Test File Name" />
   	  <input type="text" name="testWt"	id="testWtID" class="field-divided" placeholder="Test Weightage" />
	</li>
    
     
   
	</ul>
	<ul class="form-style-1">
		<li>
	   		<input type="button" name="addTestCase" value="Add TestCase" id="addTestCaseID" /> 
		</li>
	</ul>
  </div>
  
  
  
</div>
<div>	<input type="button" name="addQuestion" value="Add Question" id="addQuestion"/>
	    <input type="submit" name="cancel" value="Cancel" />
</div>

<!-- <input type="hidden" name="testCaseList" id="testCaseList" value=""> -->

</form>


<script>
    var editor = ace.edit("editor");
    editor.setTheme("ace/theme/textmate");
    editor.session.setMode("ace/mode/java");
    
    
    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
 	
</script>

</html>