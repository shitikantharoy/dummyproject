<!doctype html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang=''>
<head>
   <meta charset='utf-8'>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="styles.css">
   <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/menu.css">
   <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/login.css">
   <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/formElement.css">
   <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/jquery-ui.css">
   <script src="${pageContext.request.contextPath}/resources/jquery.js"></script>
   <script src="${pageContext.request.contextPath}/resources/jquery-ui.js"></script>
   <script src="${pageContext.request.contextPath}/resources/script.js"></script>
   <title>InCTLI</title>

<script>
	$(function() {
		$("#privilegeID").selectmenu();
	});
</script>
</head>
<body>
<div class="container">
<div class="mainHeader">
</div>
<div id='cssmenu'>
<ul>
   <li  class='has-sub'><a href='#'><span>Admin</span></a>
		 <ul>
         <li ><a href='<c:url value='/user?val=addUser'/>'><span>Add User</span></a></li>
         <li ><a href='#'><span>Edit User</span></a></li>
		 <li ><a href='#'><span>Search User</span></a></li>
      </ul>
   </li>
   <li  class='has-sub'><a href='#'><span>Questions</span></a>
		 <ul>
         <li ><a href='<c:url value='/question?val=addQuestion'/>'><span>Add Questions</span></a></li>
         <li ><a href='#'><span>Edit Questions</span></a></li>
		 <li ><a href='#'><span>Search Questions</span></a></li>
      </ul>
   </li>
   <li  class='has-sub'><a href='#'><span>Events</span></a>
		 <ul>
         <li ><a href='#'><span>Add Event</span></a></li>
         <li ><a href='#'><span>Edit Event</span></a></li>
		 <li ><a href='#'><span>Search Event</span></a></li>
      </ul>
   </li>
   <li><a href='#'><span>Reports</span></a></li>
   <li><a href='#'><span>Static</span></a></li>
</ul>
</div>

<c:if test="${userSelect=='addUser'}">
		<div>
			<jsp:include page="addUser.jsp"></jsp:include>
		</div>
</c:if>

<c:if test="${userSelect=='addQuestion'}">
		<div>
			<jsp:include page="addQuestion.jsp"></jsp:include>
		</div>
</c:if>

</div>
</body>
</html>
