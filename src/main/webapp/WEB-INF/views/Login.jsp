<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<meta charset="UTF-8">
      <title>InCTLI</title>
      <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/login.css">
      
      
      <script type="text/javascript">
        function reset()
        {
         /*    var cuid = document.getElementById('loginForm:cuid');
            cuid.select();
            cuid.focus();
            document.getElementById('reset').click(); */
        }
      </script>
    </head>
    <body onload="reset()">
			<div class="container">
			 <div class="mainHeader">
				<div></div>
			</div>
	      <h1 align="center"><font face=Calibri>Welcome to CenturyLink</font></h1>
			
				<section id="content">
	      <form id="loginForm" method="post" action="<c:url value='/validate' />" enctype="application/x-www-form-urlencoded">

	      				<h1>Login Form</h1>

			<div class="form-row">

				<label >Username&nbsp;&nbsp;</label> 
				<input type="text" name="uName" id ="uNameId"  size="13"/>
			</div>

			<div class="form-row">
				&nbsp;<label>Password&nbsp;&nbsp; </label> 
                <input type="password" name="password" size="13" id="passwordId"/>                        
			</div>

			
			<div class="form-row">			
				 <input type="submit" name="login" value="Login" />
				 <input type="submit" name="clear" value="Clear" />
				
			</div>
			 <div align="center">
				 <font face=Calibri>
					 <table></table>
				 </font> 
			 </div>
                    
                     
        
       </form></section></div>
      	
      	<div id="footer" >
				&#169; 2017   CenturyLink India, Inc. All Rights Reserved. 
		</div>
 </div>
    
</body>
  
</html>
