<%@page session="false"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Interview Login Page</title>

<c:url var="home" value="/" scope="request" />

<spring:url value="/resources/css/hello.css" var="coreCss" />
<spring:url value="/resources/css/bootstrap.min.css" var="bootstrapCss" />
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />

<spring:url value="/resources/core/js/jquery.1.10.2.min.js"
	var="jqueryJs" />
<script src="${jqueryJs}"></script>
</head>

<nav class="navbar navbar-inverse">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">Interview Assist - Candidate
				Login Page</a>
		</div>
	</div>
</nav>

<div class="container" style="min-height: 500px">

	<div class="starter-template">
		<h1>Search Form</h1>
		<br>

		<div id="feedback"></div>

		<!-- <form class="form-horizontal" id="search-form"> -->
		<%-- <form name='interview' action="<c:url value='/renderquestions' />" method='POST'> --%>
		<form:form action="renderquestions" modelAttribute="candidateForm">
			<div class="form-group form-group-lg">
				<label class="col-sm-2 control-label">Name</label>
				<div class="col-sm-10">
					<input type=text class="form-control" id="name" name="name">
				</div>
			</div>
			<div class="form-group form-group-lg">
				<label class="col-sm-2 control-label">Email</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="email" name="email">
				</div>
			</div>

			<div class="form-group form-group-lg">
				<label class="col-sm-2 control-label">Phone</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="phone" name="phone">
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" id="bth-search"
						class="btn btn-primary btn-lg">Login</button>
				</div>
			</div>
			
			<input type="hidden" name="event" value="<c:out value="${event}"/>"/>
			
		</form:form>
		<!-- </form> -->

	</div>

</div>

<div class="container">
	<footer>
		<p>
			&copy; <a href="http://www.centurylink.com">Centurylink</a> 2017
		</p>
	</footer>
</div>

<script>
	jQuery(document).ready(function($) {

		$("#search-form").submit(function(event) {

			// Disble the search button
			enableSearchButton(false);

			// Prevent the form from submitting via the browser.
			event.preventDefault();

			searchViaAjax();

		});

	});

	function jQFormSerializeArrToJson(formSerializeArr) {
		var jsonObj = {};
		jQuery.map(formSerializeArr, function(n, i) {
			jsonObj[n.name] = n.value;
		});

		return jsonObj;
	}

	function searchViaAjax() {

		var search = {}
		search["username"] = $("#username").val();
		search["email"] = $("#email").val();

		//alert (JSON.stringify(search));

		var userJson = $('#search-form').serializeArray();

		var jsonstring = jQFormSerializeArrToJson(userJson);

		alert(JSON.stringify(search) + "\n\n" + jsonstring + "\n\n" + JSON.stringify(jsonstring));

		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : "${home}addNewUser",
			//data : JSON.stringify(search),
			data : JSON.stringify(jsonstring),
			dataType : 'json',
			timeout : 100000,
			success : function(data) {
				console.log("SUCCESS: ", data);
				display(data);
			},
			error : function(e) {
				console.log("ERROR: ", e);
				display(e);
			},
			done : function(e) {
				console.log("DONE");
				enableSearchButton(true);
			}
		});

	}

	function enableSearchButton(flag) {
		$("#btn-search").prop("disabled", flag);
	}

	function display(data) {
		var json = "<h4>Ajax Response</h4><pre>" + JSON.stringify(data, null, 4) + "</pre>";
		$('#feedback').html(json);
	}
</script>

</body>
</html>