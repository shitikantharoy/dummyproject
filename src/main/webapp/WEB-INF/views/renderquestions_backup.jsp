<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<html>
 
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  	<title>Interview Questions</title>
  	<script src="${pageContext.request.contextPath}/resources/ace.js" type="text/javascript" charset="utf-8"></script>    
	<script src="${pageContext.request.contextPath}/resources/jquery.validate.js" type="text/javascript" charset="utf-8"></script>
	<script src="${pageContext.request.contextPath}/resources/jquery.js"></script>
    <script src="${pageContext.request.contextPath}/resources/jquery-ui.js"></script>
</head>
 
<%-- <body bgcolor='#<c:out value="${colorcode}" />'> --%>
<body>
 
<script> 
$(document).ready(function(){
	
	$("#compile").click(function(){
		$("#action").val("compile");
    });

	$("#next").click(function(){
		$("#action").val("next");
    });

	$("#previous").click(function(){
		$("#action").val("previous");
    });
	
	$("#exit").click(function(){
		$("#action").val("exit");
    });
	
}); 
</script> 
 
Greeting by <b><c:out value="${uniqueuser}" /></b><br/>
on <c:out value="<%=new java.util.Date()%>" /><br/>

questions are being rendered ....
<%--<c:forEach items="${questions}" var="question">
	<c:out value="${question}"/>
</c:forEach>--%>

	<c:out value="${displayQuestion}"/>

<form:form action="renderquestions" modelAttribute="candidateForm"> 

<!-- <input type="text" id="testCode" name="testFormList[0].testCode" />
<input type="text" id="testFileName" name="testFormList[0].testFileName"/>
<input type="text" id="testWeightage" name="testFormList[0].testWeightage" /> -->

								
								
<input type="text" name="answer" id="answer"/>								


<input type="hidden" id="action" name="action"/>			
<input type="hidden" id="currentIndex" name="currentIndex" value="<c:out value="${currentIndex}"/>"/>						

<button type="submit" id="compile" name="compile">Compile</button>	
<button type="submit" id="next" name="next">Next</button>	
<button type="submit" id="previous" name="previous">Previous</button>	
<button type="submit" id="exit" name="exit">Exit</button>	

</form:form>
 
</body>
</html>




