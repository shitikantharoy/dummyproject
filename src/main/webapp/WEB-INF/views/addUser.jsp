<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<script>

$(function() {
	$("#privilegeID").selectmenu();
	
    $('#addUserID').click(function(event) {
    	var userJson =$('#userForm').serializeArray();
    	var properJsonObj = jQFormSerializeArrToJson(userJson);
        alert(properJsonObj+"\n\n"+JSON.stringify(properJsonObj));
        $.ajax({
            type: "POST",
            url: "/InterviewAssist/addUserDtls",
            data:  JSON.stringify(properJsonObj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data){
                 alert(data);//handle it in a proper way
            },
            failure: function(errMsg) {
               alert(errMsg);//handle it in a proper way
            }
        });
        return false;
    });
});


function jQFormSerializeArrToJson(formSerializeArr){
 var jsonObj = {};
 jQuery.map( formSerializeArr, function( n, i ) {
     jsonObj[n.name] = n.value;
 });

 return jsonObj;
}

</script>

<form id="userForm" method="post" action="#">
<ul class="form-style-2">
	<li>
        <label>User Name <span class="required">*</span></label>
        <input type="text" name="uname"	id="nameID" placeholder="User Name"/>
    </li>
    <li>
       <label>CUID <span class="required" style="margin-right: 37px;">*</span></label>
        <input type="text"	name="cuid" id="cuidId" placeholder="CUID" /> 
    </li>
    
     <li>
        <label style="margin-right: 50px;">Privilege:</label> 
		<select name="privilege" id="privilegeID">
		      <option>Admin</option>
		      <option>Non_Admin</option>
   		</select> 
    </li>
    
    <li>
   	 	<input type="button" name="login" value="AddUser" id="addUserID" /> 
   	 	<input type="submit" name="cancel" value="Cancel" />
	</li>
	
	</ul>
</form>

</html>