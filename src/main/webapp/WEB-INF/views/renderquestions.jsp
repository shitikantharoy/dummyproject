<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!doctype html>

<html lang=''>
<head>
<meta charset='utf-8'>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="styles.css">


<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/interviewTool.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/jquery-ui.css">
<script src="${pageContext.request.contextPath}/resources/jquery.js"></script>
<script src="${pageContext.request.contextPath}/resources/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/resources/script.js"></script>
<title>InCTLI</title>


</head>
<body onload="clock();" onkeydown="return (event.keyCode != 116)">
	<div class="container">
		<div class="mainHeader"></div>





		<div>

			<!DOCTYPE html>
			<html>
<head>
<script src="${pageContext.request.contextPath}/resources/ace.js"
	type="text/javascript" charset="utf-8"></script>
</head>

<form:form action="renderquestions" modelAttribute="candidateForm">

	<div class="error" style="display: none; color: red; margin-top: 10px;">
		<img src="/InterviewAssist/resources/warning.gif" alt="Warning!"
			width="24" height="24"
			style="float: left; margin: -5px 10px 0px 0px;"> <span></span>
		<br clear="all">
	</div>


	<div>
		<div class="form-row" style="width: 80%">
			<label>Name:- </label>
			<c:out value="${uniqueuser}" />
		</div>
		<div class="form-row">
			<label>Total Questions:- </label> 30
		</div>
		<div class="emptyDIV"></div>
		<div class="form-row">
			<label>Remaining Time</label><b></b> :<span id="counter"
				style="color: #cc0000; font-weight: bold;">?</span>
		</div>
	</div>

	<div class="emptyDIV"></div>



	<div class="form-row">
		<div style="width: 500px;">
			<c:out value="${candidateForm.displayQuestion.questionDesc}" />
			<%-- <c:out value="${candidateForm.optionType}"/>&nbsp; 
				 	<c:out value="${candidateForm.currentIndex}" />&nbsp; 
				 	<c:out value="${candidateForm.lastQuestion}" />&nbsp;  --%>
		</div>

		<c:if
			test="${candidateForm.displayQuestion.questionType != 'Programming'}">

			<c:if test="${candidateForm.optionType == 'Multiple' }">
				<div id="multipleChoice">
					<label class="levels">Applicability <span class="required">*</span></label>

					<c:forEach items="${candidateForm.options}" var="answer"
						varStatus="status">
						<input type="checkbox" id="checkBox_${status.index}"
							name="answerOptions" value="${status.count}" />
						<c:out value='${answer}' />
						<br>
					</c:forEach>

					<!-- <input type="checkbox" id="validCheckBox_1" name="role" value="SE" placeholder="Software Engineeer">Value 1<br>
					<input type="checkbox" id="validCheckBox_2" name="role"	value="SSE" placeholder="Senior Software Engineeer">Value 2 <br>
					<input type="checkbox" id="validCheckBox_3" name="role" value="ML" placeholder="Module Lead">Value 3 <br>
					<input type="checkbox" id="validCheckBox_4" name="role" value="TL" placeholder="Technical Lead">Value 4 <br>
					<input type="checkbox" id="validCheckBox_5" name="role" value="PL" placeholder="Project Lead">Value 5 <br> -->
				</div>
			</c:if>

			<c:if test="${candidateForm.optionType == 'Single' }">
				<div id="singleChoice">
					<label class="levels">Option Type &nbsp; <span
						class="required">*</span></label>

					<c:forEach items="${candidateForm.options}" var="answer"
						varStatus="status">
						<input type="radio" id="radio_${status.index}"
							name="answerOptions" value="${status.count}" />
						<c:out value='${answer}' />
						<br>
					</c:forEach>

					<!-- <input type="radio" id="singleChoiceAns" name="singleChoiceAns" value="1" />Value 1 <br>
					<input type="radio" id="singleChoiceAns" name="singleChoiceAns" value="1" />Value 2 -->
				</div>
			</c:if>

		</c:if>

	</div>

	<c:if
		test="${candidateForm.displayQuestion.questionType == 'Programming'}">
		<div id="editor"
			style="overflow: scroll; position: relative; height: 450px; width: 730px; margin-top: 15px;"></div>
	</c:if>

	<div>
		<c:if
			test="${CompilationMessage != null && candidateForm.displayQuestion.questionType == 'Programming' && CompilationMessage != 'SUCCESS'}">
			<label style="color: red; font-weight: bold;">Compilation
				Message:- <c:out value="${CompilationMessage}" />
			</label>
		</c:if>
	</div>
	<div>
		<c:if
			test="${CompilationMessage != null && candidateForm.displayQuestion.questionType == 'Programming' && CompilationMessage == 'SUCCESS'}">
			<label style="color: green; font-weight: bold;">Compilation
				Message:- <c:out value="${CompilationMessage}" />
			</label>
		</c:if>
	</div>
	<div>
		<c:if
			test="${ValidationMessage != null && candidateForm.displayQuestion.questionType == 'Programming'}">
			<label style="color: green; font-weight: bold;">Message:- <c:out
					value="${ValidationMessage}" />
			</label>
		</c:if>
	</div>
		</div>




		<div>
			<!-- <input type="button" name="addQuestion" value="Add Question" id="addQuestion" /> 
		<input type="submit" name="cancel" value="Cancel" /> -->

			<c:if
				test="${candidateForm.displayQuestion.questionType == 'Programming'}">
				<input type="submit" name="compile" value="compile" id="compile" />
				<input type="submit" name="validate" value="Run Code" id="validate" />
			</c:if>
			<c:if test="${candidateForm.lastQuestion != true}">
				<input type="submit" name="next" value="next" id="next" />
			</c:if>
			<c:if test="${candidateForm.currentIndex != 0}">
				<input type="submit" name="previous" value="previous" id="previous" />
			</c:if>
			<input type="submit" name="exit" value="exit" id="exit" />

			<!-- <button type="submit" id="compile" name="compile">Compile</button>	
		<button type="submit" id="next" name="next">Next</button>	
		<button type="submit" id="previous" name="previous">Previous</button>	
		<button type="submit" id="exit" name="exit">Exit</button>	 -->
		</div>

		<input type="hidden" name="answer" id="answer" /> <input
			type="hidden" id="action" name="action" /> <input type="hidden"
			id="currentIndex" name="currentIndex"
			value="<c:out value="${candidateForm.currentIndex}"/>" /> <input
			type="hidden" id="programCode" name="programCode" /> <input
			type="hidden" id="Progcode" name="Progcode"
			value="<c:out value="${candidateForm.displayQuestion.template_code}"/>" />


		</form:form>


		<script>
    var editor = ace.edit("editor");
    editor.setTheme("ace/theme/textmate");
    editor.session.setMode("ace/mode/java");
    
    
    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
 	
</script>

		<script type="text/javascript">
                
             window.onbeforeunload = function () {   
            }
             function uncheckAll(){
                 /*  var elLength = document.getElementById("_id12").elements.length;
                     for (i=0; i<elLength; i++)
                     {
                         var type = document.getElementById("_id12").elements[i].type;
                         if (type=="checkbox"){
                             document.getElementById("_id12").elements[i].checked = false;
                         }
                     } */
             }
            
             ////////// CONFIGURE THE COUNTDOWN SCRIPT HERE //////////////////

            var month = '0';     //  '*' for next month, '0' for this month or 1 through 12 for the month 
            var day = '+0';        //  Offset for day of month day or + day  
            var hour = 20;        //  0 through 23 for the hours of the day
            var tz = +5.5;          //  Offset for your timezone in hours from UTC
            var lab = 'counter';      //  The id of the page entry where the timezone countdown is to show
            var minutes = '20';
            var secs = '10';
            var availableMins = '10';
            function start() {
                displayTZCountDown(setTZCountDown(month,day,hour,tz,minutes,secs),lab);
            }

            // **    The start function can be changed if required   **
            //window.onload = start;

            function setTZCountDown(month,day,hour,tz,minutes,secs){

                var toDate = new Date();
                if (month == '*')toDate.setMonth(toDate.getMonth() + 1);
                else if (month > 0) 
                { 
                if (month <= toDate.getMonth())toDate.setYear(toDate.getYear() + 1);
                toDate.setMonth(month-1);
                }
                if (day.substr(0,1) == '+') 
                {var day1 = parseInt(day.substr(1));
                toDate.setDate(toDate.getDate()+day1);
                } 
                else{toDate.setDate(day);
                }
                toDate.setHours(hour);
                toDate.setMinutes(minutes + availableMins);
                toDate.setSeconds(secs);
                
                var fromDate = new Date();
                var diffDate = new Date(0);
                
                /*var examEndTime = new Date();
                examEndTime.setHours(0);
                examEndTime.setMinutes(0);
                examEndTime.setSeconds(0);
                
                if(examEndTime < toDate){
                    diffDate.setMilliseconds(examEndTime - fromDate);
                }else{*/
                    diffDate.setMilliseconds(toDate - fromDate);
                //}
                
                return Math.floor(diffDate.valueOf()/1000);
            }

            function displayTZCountDown(countdown,tzcd) {

			totalQuestionsCount =2;

                if (countdown < 0) {
                    document.getElementById(tzcd).innerHTML = "Sorry, Time Over."; 
                    //alert("Sorry, Times Up!!");
                    // submit the form and log him out also
                    process();
                    return;
                }else {
                    var secs = countdown % 60; 
                }
                if (secs < 10) {
                    secs = '0'+secs;
                }
                var countdown1 = (countdown - secs) / 60;
                var mins = countdown1 % 60; 
                if (mins < 10) {
                    mins = '0'+mins;
                }
                countdown1 = (countdown1 - mins) / 60;
                var hours = countdown1 % 24;
                var days = (countdown1 - hours) / 24;
                document.getElementById(tzcd).innerHTML = hours+ 'h : ' +mins+ 'm : '+secs+'s';
             
               var remMins = Math.round(totalQuestionsCount* 0.3)-1; 
               var remMins1 = Math.round(totalQuestionsCount* 0.3);
              
               if(remMins >=1){
                 if(mins <= remMins && mins >= 1 ){
                     document.getElementById('infoMessage').innerHTML = 'Please hurry to complete the test!';
                    }
               }
                if(mins == 0){
                    document.getElementById('infoMessage').innerHTML = 'You have less than a minute to complete the test!!'; 
                   
                } 
                setTimeout('displayTZCountDown('+(countdown-1)+',\''+tzcd+'\');',1000);
            }

         </script>

		<script type="text/javascript">
            <!-- Standard Scroll Clock by kurt.grigg@virgin.net -->
            var H='....';
            var H=H.split('');
            var M='.....';
            var M=M.split('');
            var S='......';
            var S=S.split('');
            var Ypos=0;
            var Xpos=0;
            var Ybase=8;
            var Xbase=8;
            var dots=12;

            function clock(){
            var time=new Date ();
            var secs=time.getSeconds();
            var sec=-1.57 + Math.PI * secs/30;
            var mins=time.getMinutes();
            var min=-1.57 + Math.PI * mins/30;
            var hr=time.getHours();
            var hrs=-1.57 + Math.PI * hr/6 + Math.PI*parseInt(time.getMinutes())/360;
            for (i=0; i < dots; ++i){
            document.getElementById("dig" + (i+1)).style.top=0-15+40*Math.sin(-0.49+dots+i/1.9).toString() + "px";
            document.getElementById("dig" + (i+1)).style.left=0-14+40*Math.cos(-0.49+dots+i/1.9).toString() + "px";
            }
            for (i=0; i < S.length; i++){
            document.getElementById("sec" + (i+1)).style.top =Ypos+i*Ybase*Math.sin(sec).toString() + "px";
            document.getElementById("sec" + (i+1)).style.left=Xpos+i*Xbase*Math.cos(sec).toString() + "px";
            }
            for (i=0; i < M.length; i++){
            document.getElementById("min" + (i+1)).style.top =Ypos+i*Ybase*Math.sin(min).toString() + "px";
            document.getElementById("min" + (i+1)).style.left=Xpos+i*Xbase*Math.cos(min).toString() + "px";
            }
            for (i=0; i < H.length; i++){
            document.getElementById("hour" + (i+1)).style.top =Ypos+i*Ybase*Math.sin(hrs).toString() + "px";
            document.getElementById("hour" + (i+1)).style.left=Xpos+i*Xbase*Math.cos(hrs).toString() + "px";
            } 
            setTimeout('clock()',50);
            }
            
            function process(){
                alert("Sorry, The Time is over.\n" + "You have answered " + totalNumOfAnsweredQs + " questions.\n"
                   + "Please wait, your score is being calculated.");
                // save and take him to results page
                //anonymousProcess('hiddenLink');                
            }
            
                                                           
                       
/* function setEditorVal(form){
               var editor = ace.edit("editor");
               var programCode = editor.getSession().getValue();
               document.getElementById('programChallenegeFormID:hiddenEditorCodeTxtValue').value = programCode;
               //alert(document.getElementById('programChallenegeFormID:hiddenEditorCodeTxtValue').value);
                editor.getSession().setValue(document.getElementById('programChallenegeFormID:hiddenEditorCodeTxtValue').value) ;
} */

          </script>


		<script type="text/javascript">
                var hour = <c:out value="${candidateForm.loginTimeHour}"/>;
                var minutes = <c:out value="${candidateForm.loginTimeMinutes}"/>;
                var secs = <c:out value="${candidateForm.loginTimeSecs}"/>;
                var totalNumOfAnsweredQs = 1;
                var totalQuestionsCount = 5;
                
                var totalTime = <c:out value="${totalTime}"/>;; 
                var availableMins = Math.round(totalTime);      
                                start(); 
               
            </script>
            
            <script>
  $(document).ready(function(){
	
		/* var progCode= document.getElementById("Progcode").value;
		if(progCode != null)
		{
			var editor = ace.edit("editor");
			editor.getSession().setValue(progCode);	
		} */
		
		$("#editor").val = $("#Progcode").val 
		
		$("#compile").click(function(){
			 var editor = ace.edit("editor");
			 $("#programCode").val(editor.getSession().getValue());
			$("#action").val("compile");
	    });
		$("#validate").click(function(){
			 var editor = ace.edit("editor");
			 $("#programCode").val(editor.getSession().getValue());
			$("#action").val("validate");
	    });
	  
	  
	$("#compile").click(function(){
		$("#action").val("compile");
    });

	$("#next").click(function(){
		$("#action").val("next");
    });

	$("#previous").click(function(){
		$("#action").val("previous");
    });
	
	$("#exit").click(function(){
		$("#action").val("exit");
    });
	
}); 
</script>

</html>
</div>


</div>
</body>
</html>
