package com.centurylink.helper;


public class AutoGraderRequest {

	private String questionId;
	private byte[] fileContent;
	private String fileName;
	private String realPath;
	private String userId;
	private int attemp;
	
	
	public String getQuestionId() {
		return questionId;
	}
	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}
	public byte[] getFileContent() {
		return fileContent;
	}
	public void setFileContent(byte[] fileContent) {
		this.fileContent = fileContent;
	}


	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public String getRealPath() {
		return realPath;
	}
	public void setRealPath(String realPath) {
		this.realPath = realPath;
	}


	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public int getAttemp() {
		return attemp;
	}
	public void setAttemp(int attemp) {
		this.attemp = attemp;
	}
	


}
