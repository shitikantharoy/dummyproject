package com.centurylink.helper;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

public class CompileJava {

	private static Logger log = Logger.getLogger(CompileJava.class.getName());

	public static String compileCode(String FullfilePath, String path,
			boolean isTest, String progFileName) throws Exception {
		int index = FullfilePath.lastIndexOf("\\");
		String classPath = FullfilePath.substring(0, index);
		String fileName = FullfilePath.substring(index + 1,
				FullfilePath.length());
		index = fileName.lastIndexOf(".");
		String testFileName = null;
		String command = null;
		if (!isTest)
			command = "javac -cp src " + FullfilePath + " -d " + classPath;
		else {
			testFileName = fileName.substring(0, index);
			command = "javac -cp " + path + "junit.jar;src " + classPath + "\\"
					+ progFileName + " " + classPath + "\\" + testFileName
					+ ".java -d " + classPath;
		}
		System.out.println(command);
		Process pro = Runtime.getRuntime().exec(command);
		pro.waitFor();
		if (pro.exitValue() == 0) {
			return "SUCCESS";
		} else {
			return errorLines(pro.getErrorStream(), classPath);
		}

	}

	protected static String errorLines(InputStream ins, String classPath)
			throws Exception {
		String line = null;
		String errorList = null;
		BufferedReader in = new BufferedReader(new InputStreamReader(ins));
		while ((line = in.readLine()) != null) {
			System.out.println(classPath);
			int index = line.lastIndexOf("\\");
			line = line.substring(index + 1, line.length());
			if (line.contains("Picked up"))
				line = null;
			if (errorList == null)
				errorList = line;
			if (line != null) {
				errorList = errorList + ", " + line;
			}
			System.out.println(line);
		}
		return errorList;
	}

}
