package com.centurylink.helper;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;


public class AutoGraderQueue {
	
	private static BlockingQueue<AutoGraderRequest> q = new LinkedBlockingQueue<AutoGraderRequest>();
	
	
	
	private AutoGraderQueue(){
		
			
	}
	
	
	
	public static void addToQueue(AutoGraderRequest obj){
		
		q.add(obj);
	}
	
	public static AutoGraderRequest getFromQueue(){
		
		return q.poll();
	}
	

}
