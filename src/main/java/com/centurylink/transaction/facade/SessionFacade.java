package com.centurylink.transaction.facade;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.centurylink.spring.dao.ApplicabilityDAO;
import com.centurylink.spring.dao.QuestionDAO;
import com.centurylink.spring.dao.RoleDAO;
import com.centurylink.spring.dao.TestCaseDAO;
import com.centurylink.spring.model.Applicability;
import com.centurylink.spring.model.Question;
import com.centurylink.spring.model.TestCase;

public class SessionFacade {

	@Autowired
	private QuestionDAO questionDAO;

	@Autowired
	private TestCaseDAO testCaseDAO;

	@Autowired
	private ApplicabilityDAO applicabilityDAO;

	@Autowired
	private RoleDAO roleDAO;

	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public void insertQuestion(Question questionData, List<Applicability> applicabilityList, List<TestCase> testCaseList) throws Exception {

		questionDAO.saveOrUpdate(questionData);
		
		System.out.println("Inserting question " + questionData.getQuestionNo());
		
/*		for(Applicability applicability : applicabilityList) {
			applicabilityDAO.insert(applicability);
		}*/
		
		/*for(TestCase testCase : testCaseList) {
			testCaseDAO.saveOrUpdate(testCase);
		}*/
		
	}

}
