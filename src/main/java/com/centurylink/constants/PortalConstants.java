package com.centurylink.constants;

public interface PortalConstants {

	public static final String COMMA = ",";
	public static final String SPACE = " ";
	public static final String DOT_DELIMETTER = "\\.";

	public static final String ONE = "1";
	public static final String TWO = "2";
	public static final String THREE = "3";
	public static final String FOUR = "4";

	public static final String YES = "Y";
	public static final String SUCCESS = "SUCCESS";

	public static final String NEXT = "NEXT";
	public static final String PREVIOUS = "PREVIOUS";
	public static final String EXIT = "EXIT";
	public static final String COMPILE = "COMPILE";
	public static final String VALIDATE = "VALIDATE";
	public static final String PROGRAMMING = "PROGRAMMING";

}
