package com.centurylink.spring.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.centurylink.spring.dao.EventDAO;
import com.centurylink.spring.dao.QuestionDAO;
import com.centurylink.spring.dao.RoleDAO;
import com.centurylink.spring.model.Event;
import com.centurylink.spring.model.Question;
import com.centurylink.spring.model.Role;
import com.mysql.jdbc.StringUtils;

public class QuestionService {

	private static Map<String,Integer> roleMap = null;
	
	private static Logger log = Logger.getLogger(QuestionService.class.getName());
	
	@Autowired
	RoleDAO roleDAO;
	
	@Autowired
	QuestionDAO questionDAO;
	
	@Autowired
	EventDAO eventDAO;
	
	static {
		/*List<Role> roleList = roleDAO.list();
		roleMap = new HashMap<Integer,String>();
		if (roleList != null && roleList.size() >0) {
			for(Role role : roleList) {
				roleMap.put(role.getRoleId(), role.getRole());
			}
		}*/
		
	}
	
	private void populateRoleMap() {
		if(roleMap == null) {
			List<Role> roleList = roleDAO.list();
			roleMap = new HashMap<String,Integer>();
			if (roleList != null && roleList.size() >0) {
				for(Role role : roleList) {
					roleMap.put(role.getRole(),role.getRoleId());
				}
			}
		} 
	}
	
	private int getRoleIdForRole(String role) {
		
		log.debug("getRoleIdForRole role : " + role);
		
		int roleId = -1;
		
		if(roleMap == null) {
			populateRoleMap();
		}
		
		if(!StringUtils.isNullOrEmpty(role)) {
			roleId = roleMap.get(role.toUpperCase());
		}
		
		log.debug("getRoleIdForRole roleid :" + roleId);
		return roleId;
		
	}
	
	public List<Question> getQuestionByEvent(String eventName) {
		
		List<Question> questionList = new ArrayList<Question>();
		
		if(!StringUtils.isNullOrEmpty(eventName)) {
			Event event = eventDAO.get(eventName);
		}
		
		return questionList;		
		
	}
	
	public List<Question> getQuestionsByRole(Question question, String role) {
		
		
		List<Question> questionList = null;
		
		int roleId = getRoleIdForRole(role);
		
		StringBuffer query = new StringBuffer("select Q from Question Q, Applicability A where ");
		query.append("Q.questionNo = A.question.questionNo");
		query.append("A.applicabilityId.roleId = " + roleId);
		
		
		questionList = questionDAO.getQuestionsByCustomQuery(query.toString());
		
		return questionList;
	}
	
	
}
