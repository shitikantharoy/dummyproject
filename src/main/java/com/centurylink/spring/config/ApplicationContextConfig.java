package com.centurylink.spring.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.centurylink.spring.dao.AnswerDAO;
import com.centurylink.spring.dao.ApplicabilityDAO;
import com.centurylink.spring.dao.CandidateAnswerDAO;
import com.centurylink.spring.dao.CandidateDAO;
import com.centurylink.spring.dao.EventDAO;
import com.centurylink.spring.dao.QuestionDAO;
import com.centurylink.spring.dao.RoleDAO;
import com.centurylink.spring.dao.TestCaseDAO;
import com.centurylink.spring.dao.UserDAO;
import com.centurylink.spring.dao.impl.AnswerDAOImpl;
import com.centurylink.spring.dao.impl.ApplicabilityDAOImpl;
import com.centurylink.spring.dao.impl.CandidateAnswerDAOImpl;
import com.centurylink.spring.dao.impl.CandidateDAOImpl;
import com.centurylink.spring.dao.impl.EventDAOImpl;
import com.centurylink.spring.dao.impl.QuestionDAOImpl;
import com.centurylink.spring.dao.impl.RoleDAOImpl;
import com.centurylink.spring.dao.impl.TestCaseDAOImpl;
import com.centurylink.spring.dao.impl.UserDAOImpl;
import com.centurylink.spring.model.Answer;
import com.centurylink.spring.model.Applicability;
import com.centurylink.spring.model.Candidate;
import com.centurylink.spring.model.CandidateAnswer;
import com.centurylink.spring.model.Event;
import com.centurylink.spring.model.Question;
import com.centurylink.spring.model.Role;
import com.centurylink.spring.model.TestCase;
import com.centurylink.spring.model.User;
import com.centurylink.spring.service.QuestionService;
import com.centurylink.transaction.facade.SessionFacade;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.centurylink.spring")
@EnableTransactionManagement
public class ApplicationContextConfig {

	@Bean(name = "viewResolver")
	public InternalResourceViewResolver getViewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

	@Bean(name = "dataSource")
	public DataSource getDataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://10.140.0.93:3306/poc");
		dataSource.setUsername("poc_user");
		dataSource.setPassword("poc_user_pass");

		return dataSource;
	}

	private Properties getHibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.show_sql", "true");
		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		return properties;
	}

	@Autowired
	@Bean(name = "sessionFactory")
	public SessionFactory getSessionFactory(DataSource dataSource) {
		LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
		sessionBuilder.addProperties(getHibernateProperties());
		sessionBuilder.addAnnotatedClasses(Question.class);
		sessionBuilder.addAnnotatedClasses(User.class);
		sessionBuilder.addAnnotatedClasses(Role.class);
		sessionBuilder.addAnnotatedClasses(Applicability.class);
		sessionBuilder.addAnnotatedClasses(TestCase.class);
		sessionBuilder.addAnnotatedClasses(Candidate.class);
		sessionBuilder.addAnnotatedClasses(Answer.class);
		sessionBuilder.addAnnotatedClasses(CandidateAnswer.class);
		sessionBuilder.addAnnotatedClasses(Event.class);
		return sessionBuilder.buildSessionFactory();
	}

	@Autowired
	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactory);

		return transactionManager;
	}

	@Autowired
	@Bean(name = "questionDAO")
	public QuestionDAO getQuestionDAO(SessionFactory sessionFactory) {
		return new QuestionDAOImpl(sessionFactory);
	}

	@Autowired
	@Bean(name = "userDAO")
	public UserDAO getUserDAO(SessionFactory sessionFactory) {
		return new UserDAOImpl(sessionFactory);
	}

	@Autowired
	@Bean(name = "testCaseDAO")
	public TestCaseDAO geTestCaseDAO(SessionFactory sessionFactory) {
		return new TestCaseDAOImpl(sessionFactory);
	}

	@Autowired
	@Bean(name = "roleDAO")
	public RoleDAO getRoleDAO(SessionFactory sessionFactory) {
		return new RoleDAOImpl(sessionFactory);
	}

	@Autowired
	@Bean(name = "applicabilityDAO")
	public ApplicabilityDAO getApplicabilityDAO(SessionFactory sessionFactory) {
		return new ApplicabilityDAOImpl(sessionFactory);
	}

	@Autowired
	@Bean(name = "candidateDAO")
	public CandidateDAO getCandidateDAO(SessionFactory sessionFactory) {
		return new CandidateDAOImpl(sessionFactory);
	}

	@Autowired
	@Bean(name = "answerDAO")
	public AnswerDAO getAnswerDAO(SessionFactory sessionFactory) {
		return new AnswerDAOImpl(sessionFactory);
	}
	
	@Autowired
	@Bean(name = "candidateAnswerDAO")
	public CandidateAnswerDAO getCandidateAnswerDAO(SessionFactory sessionFactory) {
		return new CandidateAnswerDAOImpl(sessionFactory);
	}
	
	@Autowired
	@Bean(name = "eventDAO")
	public EventDAO getEventDAO(SessionFactory sessionFactory) {
		return new EventDAOImpl(sessionFactory);
	}

	@Bean(name = "sessionFacade")
	public SessionFacade getSessionFacade() {
		return new SessionFacade();
	}
	
	@Bean(name = "questionService")
	public QuestionService getQuestionService() {
		return new QuestionService();
	}


}
