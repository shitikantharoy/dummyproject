package com.centurylink.spring.form;

public class TestForm {

	String testCode;
	String testFileName;
	String testWeightage;

	public String getTestCode() {
		return testCode;
	}

	public void setTestCode(String testCode) {
		this.testCode = testCode;
	}

	public String getTestFileName() {
		return testFileName;
	}

	public void setTestFileName(String testFileName) {
		this.testFileName = testFileName;
	}

	public String getTestWeightage() {
		return testWeightage;
	}

	public void setTestWeightage(String testWeightage) {
		this.testWeightage = testWeightage;
	}

	@Override
	public String toString() {
		return "TestForm [testCode=" + testCode + ", testFileName=" + testFileName + ", testWeightage=" + testWeightage
				+ "]";
	}

}
