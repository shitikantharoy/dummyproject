package com.centurylink.spring.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.centurylink.spring.dao.ApplicabilityDAO;
import com.centurylink.spring.dao.QuestionDAO;
import com.centurylink.spring.dao.UserDAO;
import com.centurylink.spring.model.Applicability;
import com.centurylink.spring.model.ApplicabilityId;
import com.centurylink.spring.model.Question;
import com.centurylink.spring.model.TestCase;
import com.centurylink.spring.model.User;
import com.centurylink.transaction.facade.SessionFacade;




@Controller
public class UserRestController {

	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private SessionFacade sessionFacade;
	
	@Autowired
	private QuestionDAO questionDAO;

	@Autowired
	private ApplicabilityDAO applicabilityDAO;
	
	
	@PostMapping(value = "/addUserDtls")
	public ResponseEntity addUser(@RequestBody UserFormBean user) {
		System.out.println("fffffffffffffffffffffffffffffff"+user.toString());
		
		User user2 = new User();
		user2.setCuid(user.getCuid());
		user2.setName(user.getUname());
		user2.setPrivilege(user.getPrivilege());
		//userDAO.saveOrUpdate(user2);
		
		//temporary stub to check transaction
		
		Question questionData = new Question();
		
		questionData.setQuestionDesc("description - rollback test");
		questionData.setQuestionTechnology("technology");
		questionData.setQuestionComplexity("complexity");
		questionData.setQuestionType("questiontype");
		questionData.setQuestionTime(10);
		questionData.setQuestionActive("A");
		questionData.setFileName("filename");
		
		List<Applicability> applicabilityList = new ArrayList<Applicability>();
		//Set<Applicability> applicabilitySet = new HashSet<Applicability>();
		ApplicabilityId applicabilityId1 = new ApplicabilityId(1);
		Applicability applicability1 = new Applicability();
		applicability1.setApplicabilityId(applicabilityId1);
		//applicability.setQuestionNo(questionData.getQuestionNo());
		//applicability.setRoleId(1);
		applicability1.setQuestion(questionData);
		applicabilityList.add(applicability1);
		
		ApplicabilityId applicabilityId2 = new ApplicabilityId(3);
		Applicability applicability2 = new Applicability();
		applicability2.setApplicabilityId(applicabilityId2);
		applicability2.setQuestion(questionData);
		//applicability2.setRoleId(3);
		applicabilityList.add(applicability2);
		
		questionData.setApplicability(applicabilityList);
		
		questionDAO.saveOrUpdate(questionData);
		/*try {
			applicabilityDAO.insert(applicability);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		List<TestCase> testCaseList = new ArrayList<TestCase>();
		TestCase testCase = new TestCase();
		testCase.setQuestionNo(questionData.getQuestionNo());
		testCase.setTestCase("testcode");
		testCase.setTestFileName("testfilename");
		testCase.setTestCaseWeightage(10);
		testCaseList.add(testCase);
		
		//System.out.println("Inserting question " + questionData.getQuestionNo());
		
		/*try {
			sessionFacade.insertQuestion(questionData, applicabilityList, testCaseList);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}*/
		
		return new ResponseEntity(user, HttpStatus.OK);
	}
}
