package com.centurylink.spring.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.centurylink.spring.dao.ApplicabilityDAO;
import com.centurylink.spring.dao.QuestionDAO;
import com.centurylink.spring.dao.RoleDAO;
import com.centurylink.spring.dao.TestCaseDAO;
import com.centurylink.spring.model.Applicability;
import com.centurylink.spring.model.Question;
import com.centurylink.spring.model.Role;
import com.centurylink.spring.model.TestCase;
import com.centurylink.transaction.facade.SessionFacade;

@Controller
public class QuestionRestController {

	@Autowired
	private QuestionDAO questionDAO;
	
	@Autowired
	private TestCaseDAO testCaseDAO;
	
	@Autowired
	private ApplicabilityDAO applicabilityDAO;
	
	@Autowired
	private RoleDAO roleDAO;

/*	@Autowired
	private SessionFacade sessionFacade;*/
		
	
	@GetMapping("/questions")
	public List getQuestions() {
		return questionDAO.list();
	}
	
	@GetMapping("/questions/{id}")
	public ResponseEntity getQuestion(@PathVariable("id") int id) {

		Question question = questionDAO.get(id);
		if (question == null) {
			return new ResponseEntity("No Question found for ID " + id, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity(question, HttpStatus.OK);
	}
	
	
	@PostMapping(value = "/addQuestionSet")
	public ResponseEntity addQuestion(@RequestBody QuestionFormBean questionForm) {
		System.out.println("fffffffffffffffffffffffffffffff"+questionForm.toString());
		
		Question questionData = new Question();
		
		questionData.setQuestionDesc(questionForm.getQuestion());
		questionData.setQuestionTechnology(questionForm.getQuestionTech());
		questionData.setQuestionComplexity(questionForm.getQuestionComp());
		questionData.setQuestionType(questionForm.getQuestionType());
		questionData.setQuestionTime(new Integer(questionForm.getqTime()));
		questionData.setQuestionActive(questionForm.getQuestionAct());
		questionData.setFileName(questionForm.getFileName());
		//questionDAO.saveOrUpdate(questionData);
		
		List<Applicability> applicabilityList = new ArrayList<Applicability>();
		
		if(questionForm.getRole().length>0){
			for(int i=0; i<questionForm.getRole().length;i++){
				Role roleVal = roleDAO.get(questionForm.getRole()[i]);
				Applicability applicability = new Applicability();
				//applicability.setQuestionNo(questionData.getQuestionNo());
				//applicability.setRoleId(roleVal.getRoleId());
				//applicabilityDAO.insert(applicability);
				applicabilityList.add(applicability);
			}
		}
		
		List<TestCase> testCaseList = new ArrayList<TestCase>();
		
		if(questionForm.getTestCode().length>0){
			for(int i=0; i<questionForm.getTestCode().length;i++){
				TestCase testCase = new TestCase();
				testCase.setQuestionNo(questionData.getQuestionNo());
				testCase.setTestCase(questionForm.getTestCode()[i]);
				testCase.setTestFileName(questionForm.getTestFileNM()[i]);
				testCase.setTestCaseWeightage((new Integer(questionForm.getTestWt()[i])));
				testCaseList.add(testCase);
				//testCaseDAO.saveOrUpdate(testCase);
			}
		}
		
		//sessionFacade.insertQuestion(questionData, applicabilityList, testCaseList);
		
		
		return new ResponseEntity(questionForm, HttpStatus.OK);
	}
	
	
	/*@PostMapping(value = "/addQuestionSet")
	public ResponseEntity addQuestion(@RequestBody String json) {
		ObjectMapper questionForm = new ObjectMapper();
		try {
			 Map<QuestionFormBean,String> newTestMap = questionForm.readValue(json, new com.fasterxml.jackson.core.type.TypeReference<Map<QuestionFormBean,String>>() {});
			System.out.println("serwerwerwerwer"+newTestMap);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ResponseEntity(questionForm, HttpStatus.OK);
	}*/
	
	
	
}
