package com.centurylink.spring.controller;

import java.util.List;

public class QuestionFormBean {
	
	private String question;
	private String questionType;
	private String questionComp;
	private String questionTech;
	private String qTime;
	private String questionAct;
	private String fileName;
	private String[] role;
	
	private String[] testCode;

	private String[] testFileNM;

	private String [] testWt;

	//private List <String>questionNo;
	
	private List <TestCaseFormBean> testCaseList;
	
	
	
	
	
	public String[] getTestCode() {
		return testCode;
	}
	public void setTestCode(String[] testCode) {
		this.testCode = testCode;
	}
	public String[] getTestFileNM() {
		return testFileNM;
	}
	public void setTestFileNM(String[] testFileNM) {
		this.testFileNM = testFileNM;
	}
	public String[] getTestWt() {
		return testWt;
	}
	public void setTestWt(String[] testWt) {
		this.testWt = testWt;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	public String getQuestionComp() {
		return questionComp;
	}
	public void setQuestionComp(String questionComp) {
		this.questionComp = questionComp;
	}
	public String getQuestionTech() {
		return questionTech;
	}
	public void setQuestionTech(String questionTech) {
		this.questionTech = questionTech;
	}
	public String getqTime() {
		return qTime;
	}
	public void setqTime(String qTime) {
		this.qTime = qTime;
	}
	public List<TestCaseFormBean> getTestCaseList() {
		return testCaseList;
	}
	public void setTestCaseList(List<TestCaseFormBean> testCaseList) {
		this.testCaseList = testCaseList;
	}
	public String getQuestionAct() {
		return questionAct;
	}
	public void setQuestionAct(String questionAct) {
		this.questionAct = questionAct;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String[] getRole() {
		return role;
	}
	public void setRole(String[] role) {
		this.role = role;
	}

}
	