package com.centurylink.spring.controller;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.centurylink.constants.PortalConstants;
import com.centurylink.helper.UploadDownloadFileHelper;
import com.centurylink.spring.dao.AnswerDAO;
import com.centurylink.spring.dao.CandidateAnswerDAO;
import com.centurylink.spring.dao.CandidateDAO;
import com.centurylink.spring.dao.QuestionDAO;
import com.centurylink.spring.dao.TestCaseDAO;
import com.centurylink.spring.form.CandidateForm;
import com.centurylink.spring.model.Answer;
import com.centurylink.spring.model.Applicability;
import com.centurylink.spring.model.ApplicabilityId;
import com.centurylink.spring.model.Candidate;
import com.centurylink.spring.model.CandidateAnswer;
import com.centurylink.spring.model.CandidateAnswerId;
import com.centurylink.spring.model.Question;
import com.centurylink.spring.service.QuestionService;
import com.centurylink.spring.model.TestCase;
import com.mysql.jdbc.StringUtils;

@Controller
public class InterviewEventController {

	public static final String NEXT = "NEXT";
	public static final String PREVIOUS = "PREVIOUS";
	public static final String EXIT = "EXIT";
	public static final String MCQ = "MCQ";
	public static final String PROGRAMMING = "Programming";

	@Autowired
	HttpServletRequest request;

	@Autowired
	CandidateDAO candidateDAO;

	@Autowired
	CandidateAnswerDAO candidateAnswerDAO;

	@Autowired
	QuestionDAO questionDAO;

	@Autowired
	AnswerDAO answerDAO;

	@Autowired
	QuestionService questionService;
	@Autowired
	TestCaseDAO testCaseDAO;

	private Map<Integer, String> questioniFileMap;

	private final String Total_Test_Cases = "Total_Test_Cases";
	private final String Total_Passed_Test_Cases = "Total_Passed_Test_Cases";
	private final String Total_Test_Weightage = "Total_Test_Weightage";
	private final String Total_Passed_Test_Weightage = "Total_Passed_Test_Weightage";

	private static Logger log = Logger.getLogger(InterviewEventController.class.getName());

	@RequestMapping(value = "/event/**", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView interviewLoginPage(@RequestParam(value = "event", required = false) String event) {

		ModelAndView model = new ModelAndView();

		System.out.println("Event .... " + event);
		// validate the event

		request.setAttribute("event", event);

		model.setViewName("interviewlogin");
		return model;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/renderquestions", method = { RequestMethod.POST })
	public ModelAndView renderQuestions(@ModelAttribute("candidateForm") CandidateForm candidateForm) {

		ModelAndView model = new ModelAndView();
		model.setViewName("interviewlogin");

		HttpSession session = request.getSession();
		String action = request.getParameter("action");
		String currentIndexStr = request.getParameter("currentIndex");
		UploadDownloadFileHelper uploadDownloadFileHelper = new UploadDownloadFileHelper();
		// String answer = request.getParameter("answer");

		int currentIndex = 0;

		if (!StringUtils.isNullOrEmpty(currentIndexStr)) {
			currentIndex = Integer.valueOf(currentIndexStr);
		}

		// System.out.println(session.getMaxInactiveInterval());
		// System.out.println(candidateForm.getTestFormList() != null ?
		// candidateForm.getTestFormList().get(0) : null);

		if (PortalConstants.EXIT.equalsIgnoreCase(action)) {
			logOffCandidate();
			model.setViewName("interviewlogin");
			return model;
		}

		else if (PortalConstants.COMPILE.equalsIgnoreCase(action)) {
			String uniqueuser = (String) session.getAttribute("uniqueuser");

			log.debug("returning user ...");
			log.debug("uniqueuser ..." + uniqueuser);

			if (StringUtils.isNullOrEmpty(uniqueuser)) {
				return model;
			}
			List<Question> questionList = (List<Question>) session.getAttribute("questionList");
			questioniFileMap = (HashMap<Integer, String>) session.getAttribute("questioniFileMap");
			Question questionOnScreen = questionList.get(currentIndex);
			try {
				String compileMsg = uploadDownloadFileHelper.compileCode(uniqueuser, questionOnScreen.getQuestionNo(),
						questionOnScreen.getFileName(), candidateForm.getProgramCode(),
						questioniFileMap.get(questionOnScreen.getQuestionNo()));
				session.setAttribute("CompilationMessage", compileMsg);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				log.error(e.getStackTrace());
				e.printStackTrace();
			}
			questionOnScreen.setTemplate_code(candidateForm.getProgramCode());
			renderCurrentQuestion(candidateForm, questionList, currentIndex, action);
		}

		else if (PortalConstants.VALIDATE.equalsIgnoreCase(action)) {
			String uniqueuser = (String) session.getAttribute("uniqueuser");

			log.debug("returning user ...");
			log.debug("uniqueuser ..." + uniqueuser);

			if (StringUtils.isNullOrEmpty(uniqueuser)) {
				return model;
			}
			List<Question> questionList = (List<Question>) session.getAttribute("questionList");
			Question questionOnScreen = questionList.get(currentIndex);
			String compileMsg;
			try {
				compileMsg = uploadDownloadFileHelper.compileCode(uniqueuser, questionOnScreen.getQuestionNo(),
						questionOnScreen.getFileName(), candidateForm.getProgramCode(),
						questioniFileMap.get(questionOnScreen.getQuestionNo()));
				session.setAttribute("CompilationMessage", compileMsg);

				if ("SUCCESS".equalsIgnoreCase(compileMsg)) {
					Map<String, Integer> testCount = fetchCountOfTestCasesPassed(
							testCaseDAO.get(questionOnScreen.getQuestionNo()), questionOnScreen.getQuestionNo());

					session.setAttribute("CompilationMessage", null);
					session.setAttribute("ValidationMessage", "Number of test cases passed is "
							+ testCount.get(Total_Passed_Test_Cases) + " Out of : " + testCount.get(Total_Test_Cases));

					Map<Integer, Object> userQuestionAnswerMap = (Map<Integer, Object>) session
							.getAttribute("userQuestionAnswerMap");
					userQuestionAnswerMap.put(questionOnScreen.getQuestionNo(), testCount);
					session.setAttribute("userQuestionAnswerMap", userQuestionAnswerMap);
				}
				questionOnScreen.setTemplate_code(candidateForm.getProgramCode());
			} catch (Exception e) {
				log.error(e.getStackTrace());
				e.printStackTrace();
			}

			renderCurrentQuestion(candidateForm, questionList, currentIndex, action);
		}

		else {

			if (session.isNew()) {
				if (candidateForm.getEmail() == null) {
					return model;
				}
				log.debug("new Session ...");

				session.setMaxInactiveInterval(-1);

				String uniqueuser = createCandidate(candidateForm);
				List<Question> questionList = populateQuestions();
				Map<Integer, Object> userQuestionAnswerMap = new HashMap<Integer, Object>();

				/*
				 * for(Question question : questionList) {
				 * questions.add(question.getQuestionDesc()); }
				 */

				// model.addObject("questions", questions);
				session.setAttribute("uniqueuser", uniqueuser);
				session.setAttribute("questionList", questionList);
				session.setAttribute("questioniFileMap", questioniFileMap);
				session.setAttribute("userQuestionAnswerMap", userQuestionAnswerMap);
				session.setAttribute("totalTime", getTotalTime(questionList));
				session.setAttribute("CompilationMessage", null);
				session.setAttribute("ValidationMessage", null);

				renderCurrentQuestion(candidateForm, questionList, currentIndex, action);
				candidateForm.setLoginTime(getCandidateLogin(uniqueuser));

				/*
				 * Question displayQuestion = displayQuestion(questionList,
				 * currentIndex, action);
				 * request.setAttribute("displayQuestion", displayQuestion);
				 * request.setAttribute("currentIndex", currentIndex);
				 */

			} else {
				String uniqueuser = (String) session.getAttribute("uniqueuser");

				log.debug("returning user ...");
				log.debug("uniqueuser ..." + uniqueuser);

				if (StringUtils.isNullOrEmpty(uniqueuser)) {
					return model;
				}
				List<Question> questionList = (List<Question>) session.getAttribute("questionList");

				String answerOptions = candidateForm.getAnswerOptions();
				if (!StringUtils.isNullOrEmpty(answerOptions)) {
					Question questionOnScreen = questionList.get(currentIndex);
					Map<Integer, Object> userQuestionAnswerMap = (Map<Integer, Object>) session
							.getAttribute("userQuestionAnswerMap");
					userQuestionAnswerMap.put(questionOnScreen.getQuestionNo(), answerOptions);
					session.setAttribute("userQuestionAnswerMap", userQuestionAnswerMap);
				}
				session.setAttribute("CompilationMessage", null);
				session.setAttribute("ValidationMessage", null);
				renderCurrentQuestion(candidateForm, questionList, currentIndex, action);

				// renderCurrentQuestion(candidateForm, questionList,
				// currentIndex, action);
				candidateForm.setLoginTime(getCandidateLogin(uniqueuser));

				/*
				 * int listSize = questionList.size(); int navigatedIndex =
				 * getNavigatedIndex(listSize, currentIndex, action); Question
				 * displayQuestion = displayQuestion(questionList,
				 * navigatedIndex, action);
				 * request.setAttribute("displayQuestion", displayQuestion);
				 * request.setAttribute("currentIndex", navigatedIndex);
				 * request.setAttribute("isLastQuestion",
				 * isLastQuestion(navigatedIndex, listSize));
				 */
			}

		}

		model.setViewName("renderquestions");
		return model;

	}

	private double getTotalTime(List<Question> questionList) {
		double totalTime = 0;

		if (questionList != null) {
			for (Question question : questionList) {
				totalTime += question.getQuestionTime();
			}
		}

		return totalTime;
	}

	private void renderCurrentQuestion(CandidateForm candidateForm, List<Question> questionList, int currentIndex,
			String action) {

		if (candidateForm != null) {

			int listSize = questionList.size();
			int navigatedIndex = getNavigatedIndex(listSize, currentIndex, action);
			Question displayQuestion = displayQuestion(questionList, navigatedIndex, action);
			candidateForm.setDisplayQuestion(displayQuestion);
			candidateForm.setCurrentIndex(navigatedIndex);
			candidateForm.setLastQuestion(isLastQuestion(navigatedIndex, listSize));
			Answer answer = null;
			if (!displayQuestion.getQuestionType().equalsIgnoreCase(PortalConstants.PROGRAMMING)) {
				answer = getAnswer(displayQuestion.getQuestionNo());
			}
			if (answer != null) {
				candidateForm.setOptionType(answer.getOptionType());
				List<String> options = new ArrayList<String>();
				if (!StringUtils.isNullOrEmpty(answer.getOption1()))
					options.add(answer.getOption1());
				if (!StringUtils.isNullOrEmpty(answer.getOption2()))
					options.add(answer.getOption2());
				if (!StringUtils.isNullOrEmpty(answer.getOption3()))
					options.add(answer.getOption3());
				if (!StringUtils.isNullOrEmpty(answer.getOption4()))
					options.add(answer.getOption4());
				if (!StringUtils.isNullOrEmpty(answer.getOption5()))
					options.add(answer.getOption5());
				candidateForm.setOptions(options);
			}

			// request.setAttribute("displayQuestion", displayQuestion);
			// request.setAttribute("currentIndex", navigatedIndex);
			// request.setAttribute("isLastQuestion",
			// isLastQuestion(navigatedIndex, listSize));
		}

	}

	private Answer getAnswer(int questionNo) {

		Answer answer = answerDAO.get(questionNo);

		return answer;
	}

	private boolean isLastQuestion(int navigatedIndex, int listSize) {

		boolean isLastQuestion = false;

		if (navigatedIndex == (listSize - 1)) {
			isLastQuestion = true;
		}

		return isLastQuestion;
	}

	private int getNavigatedIndex(int size, int currentIndex, String action) {

		int updatedIndex = 0;

		if (PortalConstants.NEXT.equalsIgnoreCase(action) && currentIndex + 1 < size) {
			updatedIndex = currentIndex + 1;
		} else if (PortalConstants.PREVIOUS.equalsIgnoreCase(action) && currentIndex - 1 >= 0) {
			updatedIndex = currentIndex - 1;
		}

		return updatedIndex;
	}

	private Question displayQuestion(List<Question> questionList, int navigatedIndex, String action) {

		Question question = null;

		if (questionList != null) {
			int size = questionList.size();

			if (PortalConstants.NEXT.equalsIgnoreCase(action) && navigatedIndex < size) {
				question = questionList.get(navigatedIndex);
			} else if (PortalConstants.PREVIOUS.equalsIgnoreCase(action) && navigatedIndex >= 0) {
				question = questionList.get(navigatedIndex);
			} else if (StringUtils.isNullOrEmpty(action)) {
				question = questionList.get(0);
			} else if (!StringUtils.isNullOrEmpty(action) && navigatedIndex < size && navigatedIndex >= 0) {
				question = questionList.get(navigatedIndex);
			}
		}
		return question;
	}

	private String createCandidate(CandidateForm candidateForm) {

		String uniqueuser = candidateForm.getName() + "_" + candidateForm.getPhone();

		Candidate candidate = new Candidate();
		candidate.setEventNm(candidateForm.getEvent());
		candidate.setLogin(new java.util.Date());
		candidate.setName(uniqueuser);

		candidateDAO.create(candidate);

		return uniqueuser;

	}

	private List<Question> populateQuestions() {

		int mcqCount = 3;
		int programCount = 2;

		List<Question> questionList = new ArrayList<Question>();

		Question questionMCQ = new Question();
		questionMCQ.setQuestionType(MCQ);
		/*
		 * List<Applicability> applicabilityList = new
		 * ArrayList<Applicability>(); ApplicabilityId applicabilityId = new
		 * ApplicabilityId(3); Applicability applicability = new
		 * Applicability(); applicability.setApplicabilityId(applicabilityId);
		 * questionMCQ.setApplicability(applicabilityList);
		 */

		List<Question> tempList1 = questionDAO.getQuestionsByCriteria(questionMCQ);
		Collections.shuffle(tempList1, new Random());

		// questionService.getQuestionsByRole(questionMCQ, "SE");

		/*
		 * if(tempList1 != null && tempList1.size() > 0) { for(int i=0; i <
		 * mcqCount; i++) { questionList.add(tempList1.get(i)); } }
		 */

		questionList.addAll(tempList1.subList(0, mcqCount));

		Collections.shuffle(questionList, new Random());

		Question questionProgramming = new Question();
		questionProgramming.setQuestionType(PROGRAMMING);

		List<Question> tempList2 = questionDAO.getQuestionsByCriteria(questionProgramming);
		Collections.shuffle(tempList2, new Random());

		/*
		 * if(tempList2 != null && tempList2.size() > 0) { for(int i=0; i <
		 * programCount; i++) { questionList.add(tempList2.get(i)); } }
		 */

		questionList.addAll(tempList2.subList(0, programCount));

		return questionList;
	}

	private Calendar getCandidateLogin(String uniqueuser) {

		Calendar loginTime = Calendar.getInstance();
		Candidate candidate = candidateDAO.get(uniqueuser);
		loginTime.setTime(candidate.getLogin());
		return loginTime;
	}

	private void generateFile(List<Question> questions, String uniqueuser) {
		for (Question question : questions) {
			if (question.getQuestionType().equalsIgnoreCase(PortalConstants.PROGRAMMING)) {
				UploadDownloadFileHelper helper = new UploadDownloadFileHelper();
				String fileFullPath = helper.createFile(uniqueuser, request.getParameter("event"),
						question.getQuestionNo(), question.getFileName(), question.getTemplate_code(), null, null,
						false);
				List<TestCase> list = testCaseDAO.get(question.getQuestionNo());
				for (TestCase testCase : list) {
					helper.createFile(uniqueuser, request.getParameter("event"), question.getQuestionNo(),
							question.getFileName(), question.getTemplate_code(), testCase.getTestFileName(),
							testCase.getTestCode(), true);
					// setProg(new String(qbank.getSkeleton_code()));
				}

				questioniFileMap.put(question.getQuestionNo(), fileFullPath);
			}
		}
	}

	private void logOffCandidate() {

		log.debug("Logging out user !!");

		HttpSession session = request.getSession();

		String uniqueuser = (String) session.getAttribute("uniqueuser");
		@SuppressWarnings("unchecked")
		Map<Integer, Object> userQuestionAnswerMap = (Map<Integer, Object>) session
				.getAttribute("userQuestionAnswerMap");

		session.invalidate();
		Candidate candidate = candidateDAO.get(uniqueuser);
		candidate.setLogout(new java.util.Date());
		candidateDAO.saveOrUpdate(candidate);

		saveCandidateAnswersToDB(userQuestionAnswerMap, candidate.getCandidateId());

	}

	@SuppressWarnings("unchecked")
	private void saveCandidateAnswersToDB(Map<Integer, Object> userQuestionAnswerMap, int candidateId) {

		if (userQuestionAnswerMap != null) {

			Set<Integer> answerSet = userQuestionAnswerMap.keySet();

			for (Integer questionNo : answerSet) {
				CandidateAnswerId candidateAnswerId = new CandidateAnswerId(candidateId, questionNo);
				CandidateAnswer candidateAnswer = new CandidateAnswer(candidateAnswerId);
				if (userQuestionAnswerMap.get(questionNo) instanceof String) {
					candidateAnswer.setCandidateAnswer(String.valueOf(userQuestionAnswerMap.get(questionNo)));
				} else {
					Map<String, Integer> programQuesRunDetails = (Map<String, Integer>) userQuestionAnswerMap
							.get(questionNo);

					for (Entry<String, Integer> entry : programQuesRunDetails.entrySet()) {
						System.out.println(entry.getKey() + "/" + entry.getValue());
						if (Total_Passed_Test_Cases.equalsIgnoreCase(entry.getKey()))
							candidateAnswer.setPassedTestCases(entry.getValue().toString());
						if (Total_Passed_Test_Weightage.equalsIgnoreCase(entry.getKey()))
							candidateAnswer.setAchievedWeightage(entry.getValue().toString());
					}
				}
				candidateAnswerDAO.insert(candidateAnswer);
			}
		}
	}

	/**
	 * @param testCasesAndOutput
	 * @return
	 * @throws Exception
	 */
	private Map<String, Integer> fetchCountOfTestCasesPassed(List<TestCase> testCases, int questionNum)
			throws Exception {
		Map<String, Integer> results = new HashMap<String, Integer>();
		List<Integer> testResult = new ArrayList<Integer>();
		for (TestCase testcase : testCases) {

			if (testResult.size() == 0) {
				testResult = runProcess(questioniFileMap.get(questionNum), testcase.getTestFileName());
				testResult.add(testResult.get(0) * testcase.getTestCaseWeightage());
				testResult.add(testResult.get(1) * testcase.getTestCaseWeightage());
			} else {
				List<Integer> result = runProcess(questioniFileMap.get(questionNum), testcase.getTestFileName());
				testResult.set(0, testResult.get(0) + result.get(0));
				testResult.set(1, testResult.get(1) + result.get(1));
				testResult.set(2, testResult.get(2) + (testResult.get(0) * testcase.getTestCaseWeightage()));
				testResult.add(3, testResult.get(3) + (testResult.get(1) * testcase.getTestCaseWeightage()));
			}
		}
		results.put(Total_Test_Cases, testResult.get(0));
		results.put(Total_Passed_Test_Cases, testResult.get(1));
		results.put(Total_Test_Weightage, testResult.get(2));
		results.put(Total_Passed_Test_Weightage, testResult.get(3));
		return results;
	}

	/**
	 * 
	 * @param filePath
	 * @param fileName
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private List<Integer> runProcess(String filePath, String fileName) throws IOException, InterruptedException {
		List<Integer> testResult = new ArrayList<Integer>();
		int index = filePath.lastIndexOf("\\");
		filePath = filePath.substring(0, index);
		if (!fileName.contains(".java")) {
			fileName = fileName + ".java";
		}
		index = fileName.lastIndexOf(".");
		fileName = fileName.substring(0, index);
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

		URLClassLoader urlClassLoader = URLClassLoader.newInstance(new URL[] { new URL("file:///" + filePath + "\\") },
				classLoader);
		try {
			Class clazz = urlClassLoader.loadClass(fileName);
			Result result = JUnitCore.runClasses(clazz);
			testResult.add(result.getRunCount());
			if (result.getFailures().isEmpty()) {
				System.out.println("Test Suite successFul");
			}
			testResult.add(result.getRunCount() - result.getFailures().size());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return testResult;
	}
}
