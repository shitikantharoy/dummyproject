package com.centurylink.spring.dao;

import java.util.List;

import com.centurylink.spring.model.Question;


public interface QuestionDAO {

	public List<Question> list();
	
	public Question get(int id);
	
	public void saveOrUpdate(Question question);
	
	public void delete(int id);

	public List<Question> findAllQuestions();
	
	public List<Question> getQuestionsByCriteria(Question question);
	
	public List<Question> getQuestionsByCustomQuery(String hql);
	
}
