package com.centurylink.spring.dao;

import com.centurylink.spring.model.User;


public interface UserDAO {

	public User get(String cuid);
	
	public void saveOrUpdate(User user);
	
//	public void create(User user);
	
	public void delete(String cuid);
	
}
