package com.centurylink.spring.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.centurylink.spring.dao.AnswerDAO;
import com.centurylink.spring.model.Answer;
import com.centurylink.spring.model.Question;

@Repository
public class AnswerDAOImpl implements AnswerDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public AnswerDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	

	@Override
	@Transactional
	public Answer get(Integer qNo) {
		String hql = "FROM Answer A WHERE A.QNo = :questionNo";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setInteger("questionNo",qNo);
		
		/*Question question = new Question();
		question.setQuestionNo(qNo);
		
		Answer answer = new Answer();
		answer.setQuestion(question);*/
		
		//Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Answer.class);
		
		@SuppressWarnings("unchecked")
		List<Answer> answerList = (List<Answer>) query.list();
		//List<Answer> answerList = (List<Answer>) sessionFactory.getCurrentSession().createQuery("select  question ");
		
		if (answerList != null && !answerList.isEmpty()) {
			return answerList.get(0);
		}
		
		return null;
	}

	@Override
	@Transactional
	public void saveOrUpdate(Answer answer) {
		sessionFactory.getCurrentSession().saveOrUpdate(answer);
	}
	
	@Transactional
	public void insert(Answer answer) {
		sessionFactory.getCurrentSession().persist(answer);
	}
	
	/*@Override
	@Transactional
	public void create(User user) {
		public Customer create(Customer customer) {
			customer.setId(System.currentTimeMillis());
			customers.add(customer);
			return customer;
		}
		sessionFactory.getCurrentSession().create(user);
	}*/

	@Override
	@Transactional
	public void delete(Integer qNo) {
		Answer answer = new Answer();
		Question question = new Question();
		question.setQuestionNo(qNo);
		answer.setQuestion(question);
		sessionFactory.getCurrentSession().delete(answer);
	}

}
