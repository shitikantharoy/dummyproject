package com.centurylink.spring.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.centurylink.spring.dao.QuestionDAO;
import com.centurylink.spring.model.Question;

@Repository
public class QuestionDAOImpl implements QuestionDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public QuestionDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public List<Question> list() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public Question get(int id) {
		String hql = "from Question where questionNo=" + id;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		@SuppressWarnings("unchecked")
		List<Question> listQuestion = (List<Question>) query.list();
		
		if (listQuestion != null && !listQuestion.isEmpty()) {
			return listQuestion.get(0);
		}
		
		return null;
	}
	
	@Override
	@Transactional
	public List<Question> getQuestionsByCriteria(Question question) {
		
		
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Question.class);
		Example questionE = Example.create(question);
		questionE.excludeZeroes();
		criteria.add(questionE);
		List<Question> questionList = criteria.list();
		
		return questionList;
	}

	@Override
	@Transactional
	public void saveOrUpdate(Question question) {
		sessionFactory.getCurrentSession().persist(question);
	}

	@Override
	@Transactional
	public void delete(int questionNo) {
		Question questionToDelete = new Question();
		questionToDelete.setQuestionNo(questionNo);
		sessionFactory.getCurrentSession().delete(questionToDelete);
	}
	
	@Override
	@Transactional
	public List<Question> findAllQuestions() {
		
		Query query = sessionFactory.getCurrentSession().getNamedQuery("Question.findAll");
		List<Question> questionList = (List<Question>) query.list();
		
		return questionList;
	}

	@Override
	@Transactional
	public List<Question> getQuestionsByCustomQuery(String hql) {
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		@SuppressWarnings("unchecked")
		List<Question> listQuestion = (List<Question>) query.list();
		
		return listQuestion;
	}

}
