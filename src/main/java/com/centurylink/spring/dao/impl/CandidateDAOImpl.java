package com.centurylink.spring.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.centurylink.spring.dao.CandidateDAO;
import com.centurylink.spring.model.Candidate;

public class CandidateDAOImpl implements CandidateDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public CandidateDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@Transactional
	public Candidate get(String uniqueid) {
		/*String hql = "from Candidate where name=" + uniqueid;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);*/
		
		Query query = sessionFactory.getCurrentSession().getNamedQuery("Candidate.findCandidate");
		query.setParameter("uniqueid", uniqueid);
		
		@SuppressWarnings("unchecked")
		List<Candidate> listCandidate = (List<Candidate>) query.list();
		
		//List<Candidate> listCandidate = (List<Candidate>) query.list();
		
		if (listCandidate != null && !listCandidate.isEmpty()) {
			return listCandidate.get(0);
		}
		
		return null;
	}

	@Override
	@Transactional
	public void saveOrUpdate(Candidate candidate) {
		sessionFactory.getCurrentSession().saveOrUpdate(candidate);
		
	}

	@Override
	@Transactional
	public void create(Candidate candidate) {
		sessionFactory.getCurrentSession().persist(candidate);
	}

	@Override
	@Transactional
	public void delete(String uniqueid) {
		Candidate candidate = new Candidate();
		candidate.setName(uniqueid);
		sessionFactory.getCurrentSession().delete(candidate);
	}

}
