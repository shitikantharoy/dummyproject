package com.centurylink.spring.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.centurylink.spring.dao.RoleDAO;
import com.centurylink.spring.model.Question;
import com.centurylink.spring.model.Role;

@Repository
public class RoleDAOImpl implements RoleDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public RoleDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	

	@Override
	@Transactional
	public Role get(String role) {
		
		String hql = "FROM Role R WHERE R.role = :role";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("role",role);
		
		@SuppressWarnings("unchecked")
		List<Role> roleLst = (List<Role>) query.list();
		
		if (roleLst != null && !roleLst.isEmpty()) {
			return roleLst.get(0);
		}
		
		return null;
	}
	

	@Override
	@Transactional
	public void saveOrUpdate(Role role) {
		sessionFactory.getCurrentSession().saveOrUpdate(role);
	}
	

	@Override
	@Transactional
	public void delete(String role) {
		Role roleObj = new Role();
		roleObj.setRole(role);
		sessionFactory.getCurrentSession().delete(roleObj);
	}


	@Override
	public List<Role> list() {
		String hql = "from Role";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		@SuppressWarnings("unchecked")
		List<Role> listRole = (List<Role>) query.list();
		return listRole;
	}

}
