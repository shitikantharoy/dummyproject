package com.centurylink.spring.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.centurylink.spring.dao.UserDAO;
import com.centurylink.spring.model.Customer;
import com.centurylink.spring.model.User;

@Repository
public class UserDAOImpl implements UserDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public UserDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	

	@Override
	@Transactional
	public User get(String cuid) {
		String hql = "from User where cuid=" + cuid;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		@SuppressWarnings("unchecked")
		List<User> listQuestion = (List<User>) query.list();
		
		if (listQuestion != null && !listQuestion.isEmpty()) {
			return listQuestion.get(0);
		}
		
		return null;
	}

	@Override
	@Transactional
	public void saveOrUpdate(User user) {
		sessionFactory.getCurrentSession().saveOrUpdate(user);
	}
	
	/*@Override
	@Transactional
	public void create(User user) {
		public Customer create(Customer customer) {
			customer.setId(System.currentTimeMillis());
			customers.add(customer);
			return customer;
		}
		sessionFactory.getCurrentSession().create(user);
	}*/

	@Override
	@Transactional
	public void delete(String cuid) {
		User user = new User();
		user.setCuid(cuid);
		sessionFactory.getCurrentSession().delete(user);
	}

}
