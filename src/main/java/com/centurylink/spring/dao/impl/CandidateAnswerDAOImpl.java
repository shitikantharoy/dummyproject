package com.centurylink.spring.dao.impl;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.centurylink.spring.dao.CandidateAnswerDAO;
import com.centurylink.spring.model.CandidateAnswer;

public class CandidateAnswerDAOImpl implements CandidateAnswerDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public CandidateAnswerDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	
	@Override
	public void saveOrUpdate(CandidateAnswer answer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional
	public void insert(CandidateAnswer candidateAnswer) {
		sessionFactory.getCurrentSession().persist(candidateAnswer);
		
	}
	
}
