package com.centurylink.spring.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.centurylink.spring.dao.ApplicabilityDAO;
import com.centurylink.spring.dao.UserDAO;
import com.centurylink.spring.model.Applicability;
import com.centurylink.spring.model.User;

@Repository
public class ApplicabilityDAOImpl implements ApplicabilityDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public ApplicabilityDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	

	@Override
	@Transactional
	public Applicability get(Integer qNo) {
		String hql = "FROM Applicability A WHERE A.questionNo = :questionNo";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("questionNo",qNo);
		
		@SuppressWarnings("unchecked")
		List<Applicability> applicabilityLst = (List<Applicability>) query.list();
		
		if (applicabilityLst != null && !applicabilityLst.isEmpty()) {
			return applicabilityLst.get(0);
		}
		
		return null;
	}

	@Override
	@Transactional
	public void saveOrUpdate(Applicability applicability) {
		sessionFactory.getCurrentSession().saveOrUpdate(applicability);
	}
	
	@Transactional
	public void insert(Applicability applicability) throws Exception {
		sessionFactory.getCurrentSession().persist(applicability);
		//throw new Exception("My custom exception");
	}
	
	/*@Override
	@Transactional
	public void create(User user) {
		public Customer create(Customer customer) {
			customer.setId(System.currentTimeMillis());
			customers.add(customer);
			return customer;
		}
		sessionFactory.getCurrentSession().create(user);
	}*/

	@Override
	@Transactional
	public void delete(Integer qNo) {
		Applicability applicability = new Applicability();
		//applicability.setQuestionNo(qNo);
		sessionFactory.getCurrentSession().delete(applicability);
	}

}
