package com.centurylink.spring.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.centurylink.spring.dao.TestCaseDAO;
import com.centurylink.spring.model.TestCase;
import com.centurylink.spring.model.User;

@Repository
public class TestCaseDAOImpl implements TestCaseDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public TestCaseDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	

	@Override
	@Transactional
	public List<TestCase> get(Integer qNo) {
		
		String hql = "FROM TestCase T WHERE T.questionNo = :questionNo";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("questionNo",qNo);
		
		@SuppressWarnings("unchecked")
		List<TestCase> listQuestion = (List<TestCase>) query.list();
		
		return listQuestion;
	}

	@Override
	@Transactional
	public void saveOrUpdate(TestCase testCase) {
		sessionFactory.getCurrentSession().saveOrUpdate(testCase);
	}
	
	/*@Override
	@Transactional
	public void create(User user) {
		public Customer create(Customer customer) {
			customer.setId(System.currentTimeMillis());
			customers.add(customer);
			return customer;
		}
		sessionFactory.getCurrentSession().create(user);
	}*/

	@Override
	@Transactional
	public void delete(Integer qNo) {
		TestCase testCase = new TestCase();
		testCase.setQuestionNo(qNo);;
		sessionFactory.getCurrentSession().delete(testCase);
	}

}
