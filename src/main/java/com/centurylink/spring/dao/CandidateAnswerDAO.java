package com.centurylink.spring.dao;

import com.centurylink.spring.model.Answer;
import com.centurylink.spring.model.CandidateAnswer;


public interface CandidateAnswerDAO {

	//public Answer get(Integer qNo);
	
	public void saveOrUpdate(CandidateAnswer answer);
	
	public void insert(CandidateAnswer candidateAnswer);
	
	//public void delete(Integer qNo);
	
}
