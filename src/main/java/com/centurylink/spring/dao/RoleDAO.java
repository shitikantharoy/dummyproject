package com.centurylink.spring.dao;

import java.util.List;

import com.centurylink.spring.model.Role;


public interface RoleDAO {

	public Role get(String role);
	
	public void saveOrUpdate(Role role);
	
	public void delete(String role);
	
	public List<Role> list();
}
