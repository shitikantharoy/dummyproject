package com.centurylink.spring.dao;

import com.centurylink.spring.model.Applicability;


public interface ApplicabilityDAO {

	public Applicability get(Integer qNo);
	
	public void saveOrUpdate(Applicability applicability);
	public void insert(Applicability applicability) throws Exception;
	
	public void delete(Integer qNo);
	
}
