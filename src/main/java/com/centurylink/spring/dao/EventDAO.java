package com.centurylink.spring.dao;

import com.centurylink.spring.model.Event;


public interface EventDAO {

	public Event get(String eventName);
	
	public void saveOrUpdate(Event event);
	
	public void create(Event event);
	
	public void delete(Event eventName);

	
}
