package com.centurylink.spring.dao;

import java.util.List;

import com.centurylink.spring.model.TestCase;
import com.centurylink.spring.model.User;


public interface TestCaseDAO {

	public List<TestCase> get(Integer qNo);
	
	public void saveOrUpdate(TestCase testCase);
	
	public void delete(Integer qNo);
	
}
