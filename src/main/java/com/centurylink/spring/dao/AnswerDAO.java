package com.centurylink.spring.dao;

import com.centurylink.spring.model.Answer;


public interface AnswerDAO {

	public Answer get(Integer qNo);
	
	public void saveOrUpdate(Answer answer);
	
	public void insert(Answer answer);
	
	public void delete(Integer qNo);
	
}
