package com.centurylink.spring.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "applicability")
public class Applicability implements Serializable {

	private static final long serialVersionUID = 1L;

	/*
	 * Qno int(11) DEFAULT NULL, Role_ID int(11) DEFAULT NULL, KEY
	 * Role_Applicability_FK_idx (Role_ID), CONSTRAINT Role_Applicability_FK
	 * FOREIGN KEY (Role_ID) REFERENCES role (Role_ID) ON DELETE NO ACTION ON
	 * UPDATE NO ACTION
	 */
	/*
	 * @Id
	 * 
	 * @Column(name = "Qno", insertable = false, updatable = false) private int
	 * questionNo;
	 */

	/*
	 * @Column(name = "Role_ID") private int roleId;
	 */
	@ManyToOne
	@JoinColumn(name = "QNo", referencedColumnName = "QNo")
	Question question;

	@EmbeddedId
	ApplicabilityId applicabilityId;

	/*
	 * @ManyToOne
	 * 
	 * @JoinColumn(name = "Role_ID", referencedColumnName = "Role_ID") private
	 * Role role;
	 */

	/*
	 * public int getRoleId() { return roleId; }
	 */

	public ApplicabilityId getApplicabilityId() {
		return applicabilityId;
	}

	public void setApplicabilityId(ApplicabilityId applicabilityId) {
		this.applicabilityId = applicabilityId;
	}

	/*
	 * public void setRoleId(int roleId) { this.roleId = roleId; }
	 */

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	

}
