package com.centurylink.spring.model;


import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "interview_event")
@NamedQueries({
		@NamedQuery(name = "event.findAll", query = "select e from Event e where e.eventStatus='ACTIVE'"),
		@NamedQuery(name = "event.findByEventName", query = "SELECT e FROM Event e WHERE e.eventStatus='ACTIVE' and e.eventName = :event_name"), })
public class Event implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "interview_event_id", nullable = false)
	private int interviewEventId;
	
	@Column(name = "role_id")
	private int roleId; 

	@Column(name = "event_start_dt")
	private Date eventStartDate;

	@Column(name = "event_end_dt")
	private Date eventEndDate;

	@Column(name = "event_start_time")
	private Time eventStartTime;

	@Column(name = "event_end_time")
	private Time eventEndTime;

	@Column(name = "event_name", nullable = false)
	private String eventName;

	@Column(name = "event_url", nullable = false)
	private String eventUrl;

	@Column(name = "event_technology", nullable = false)
	private String eventTechnology;

	@Column(name = "event_status", nullable = false)
	private String eventStatus;

	@Column(name = "simple_mcq")
	private String simpleMcq;
		
	@Column(name = "medium_mcq")
	private String mediumMcq;

	@Column(name = "complex_mcq")
	private String complexMcq;	

	@Column(name = "simple_pgm")
	private String simplePgm;	
	
	@Column(name = "medium_pgm")
	private String mediumPgm;
	
	@Column(name = "complex_pgm")
	private String complexPgm;
	
	@Column(name="modified_dt")
	private Timestamp modifiedDt;

	public int getInterviewEventId() {
		return interviewEventId;
	}

	public void setInterviewEventId(int interviewEventId) {
		this.interviewEventId = interviewEventId;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public Date getEventStartDate() {
		return eventStartDate;
	}

	public void setEventStartDate(Date eventStartDate) {
		this.eventStartDate = eventStartDate;
	}

	public Date getEventEndDate() {
		return eventEndDate;
	}

	public void setEventEndDate(Date eventEndDate) {
		this.eventEndDate = eventEndDate;
	}

	public Time getEventStartTime() {
		return eventStartTime;
	}

	public void setEventStartTime(Time eventStartTime) {
		this.eventStartTime = eventStartTime;
	}

	public Time getEventEndTime() {
		return eventEndTime;
	}

	public void setEventEndTime(Time eventEndTime) {
		this.eventEndTime = eventEndTime;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventUrl() {
		return eventUrl;
	}

	public void setEventUrl(String eventUrl) {
		this.eventUrl = eventUrl;
	}

	public String getEventTechnology() {
		return eventTechnology;
	}

	public void setEventTechnology(String eventTechnology) {
		this.eventTechnology = eventTechnology;
	}

	public String getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
	}

	public String getSimpleMcq() {
		return simpleMcq;
	}

	public void setSimpleMcq(String simpleMcq) {
		this.simpleMcq = simpleMcq;
	}

	public String getMediumMcq() {
		return mediumMcq;
	}

	public void setMediumMcq(String mediumMcq) {
		this.mediumMcq = mediumMcq;
	}

	public String getComplexMcq() {
		return complexMcq;
	}

	public void setComplexMcq(String complexMcq) {
		this.complexMcq = complexMcq;
	}

	public String getSimplePgm() {
		return simplePgm;
	}

	public void setSimplePgm(String simplePgm) {
		this.simplePgm = simplePgm;
	}

	public String getMediumPgm() {
		return mediumPgm;
	}

	public void setMediumPgm(String mediumPgm) {
		this.mediumPgm = mediumPgm;
	}

	public String getComplexPgm() {
		return complexPgm;
	}

	public void setComplexPgm(String complexPgm) {
		this.complexPgm = complexPgm;
	}

	public Timestamp getModifiedDt() {
		return modifiedDt;
	}

	public void setModifiedDt(Timestamp modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((complexMcq == null) ? 0 : complexMcq.hashCode());
		result = prime * result + ((complexPgm == null) ? 0 : complexPgm.hashCode());
		result = prime * result + ((eventEndDate == null) ? 0 : eventEndDate.hashCode());
		result = prime * result + ((eventEndTime == null) ? 0 : eventEndTime.hashCode());
		result = prime * result + ((eventName == null) ? 0 : eventName.hashCode());
		result = prime * result + ((eventStartDate == null) ? 0 : eventStartDate.hashCode());
		result = prime * result + ((eventStartTime == null) ? 0 : eventStartTime.hashCode());
		result = prime * result + ((eventStatus == null) ? 0 : eventStatus.hashCode());
		result = prime * result + ((eventTechnology == null) ? 0 : eventTechnology.hashCode());
		result = prime * result + ((eventUrl == null) ? 0 : eventUrl.hashCode());
		result = prime * result + interviewEventId;
		result = prime * result + ((mediumMcq == null) ? 0 : mediumMcq.hashCode());
		result = prime * result + ((mediumPgm == null) ? 0 : mediumPgm.hashCode());
		result = prime * result + ((modifiedDt == null) ? 0 : modifiedDt.hashCode());
		result = prime * result + roleId;
		result = prime * result + ((simpleMcq == null) ? 0 : simpleMcq.hashCode());
		result = prime * result + ((simplePgm == null) ? 0 : simplePgm.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Event other = (Event) obj;
		if (complexMcq == null) {
			if (other.complexMcq != null)
				return false;
		} else if (!complexMcq.equals(other.complexMcq))
			return false;
		if (complexPgm == null) {
			if (other.complexPgm != null)
				return false;
		} else if (!complexPgm.equals(other.complexPgm))
			return false;
		if (eventEndDate == null) {
			if (other.eventEndDate != null)
				return false;
		} else if (!eventEndDate.equals(other.eventEndDate))
			return false;
		if (eventEndTime == null) {
			if (other.eventEndTime != null)
				return false;
		} else if (!eventEndTime.equals(other.eventEndTime))
			return false;
		if (eventName == null) {
			if (other.eventName != null)
				return false;
		} else if (!eventName.equals(other.eventName))
			return false;
		if (eventStartDate == null) {
			if (other.eventStartDate != null)
				return false;
		} else if (!eventStartDate.equals(other.eventStartDate))
			return false;
		if (eventStartTime == null) {
			if (other.eventStartTime != null)
				return false;
		} else if (!eventStartTime.equals(other.eventStartTime))
			return false;
		if (eventStatus == null) {
			if (other.eventStatus != null)
				return false;
		} else if (!eventStatus.equals(other.eventStatus))
			return false;
		if (eventTechnology == null) {
			if (other.eventTechnology != null)
				return false;
		} else if (!eventTechnology.equals(other.eventTechnology))
			return false;
		if (eventUrl == null) {
			if (other.eventUrl != null)
				return false;
		} else if (!eventUrl.equals(other.eventUrl))
			return false;
		if (interviewEventId != other.interviewEventId)
			return false;
		if (mediumMcq == null) {
			if (other.mediumMcq != null)
				return false;
		} else if (!mediumMcq.equals(other.mediumMcq))
			return false;
		if (mediumPgm == null) {
			if (other.mediumPgm != null)
				return false;
		} else if (!mediumPgm.equals(other.mediumPgm))
			return false;
		if (modifiedDt == null) {
			if (other.modifiedDt != null)
				return false;
		} else if (!modifiedDt.equals(other.modifiedDt))
			return false;
		if (roleId != other.roleId)
			return false;
		if (simpleMcq == null) {
			if (other.simpleMcq != null)
				return false;
		} else if (!simpleMcq.equals(other.simpleMcq))
			return false;
		if (simplePgm == null) {
			if (other.simplePgm != null)
				return false;
		} else if (!simplePgm.equals(other.simplePgm))
			return false;
		return true;
	}
	
	
}
