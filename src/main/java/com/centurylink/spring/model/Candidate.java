package com.centurylink.spring.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "candidate")
@NamedQueries(value = {
		@NamedQuery(name = "Candidate.findCandidate", query = "select c from Candidate c where c.name=:uniqueid") })
public class Candidate {

	/*
	 * Candidate_ID int(11) NOT NULL, Event_NM varchar(45) DEFAULT NULL, Name
	 * varchar(45) DEFAULT NULL, Login timestamp NULL DEFAULT NULL, Logout
	 * timestamp NULL DEFAULT NULL, PRIMARY KEY (Candidate_ID)
	 */

	@Id
	@Column(name = "Candidate_ID")
	private int candidateId;

	@Column(name = "Event_NM")
	private String eventNm;

	@Column(name = "Name")
	private String name;

	@Column(name = "Login")
	private Date login;

	@Column(name = "Logout")
	private Date logout;

	public int getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(int candidateId) {
		this.candidateId = candidateId;
	}

	public String getEventNm() {
		return eventNm;
	}

	public void setEventNm(String eventNm) {
		this.eventNm = eventNm;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getLogin() {
		return login;
	}

	public void setLogin(Date login) {
		this.login = login;
	}

	public Date getLogout() {
		return logout;
	}

	public void setLogout(Date logout) {
		this.logout = logout;
	}

}
