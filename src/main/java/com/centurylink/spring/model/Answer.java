package com.centurylink.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "answer")
public class Answer {

	/*
	 * Answer_ID int(11) NOT NULL, Option_1 varchar(100) DEFAULT NULL, Option_2
	 * varchar(100) DEFAULT NULL, Option_3 varchar(100) DEFAULT NULL, Option_4
	 * varchar(100) DEFAULT NULL, Option_5 varchar(100) DEFAULT NULL,
	 * Correct_Answer varchar(10) DEFAULT NULL, QNo int(11) DEFAULT NULL,
	 * PRIMARY KEY (Answer_ID), KEY Ques_Ans_FK_idx (QNo), CONSTRAINT
	 * Ques_Ans_FK FOREIGN KEY (QNo) REFERENCES question (QNo) ON DELETE NO
	 * ACTION ON UPDATE NO ACTION
	 */

	@Id
	@Column(name = "Answer_ID")
	int answerId;

	@Column(name = "Option_1")
	String option1;

	@Column(name = "Option_2")
	String option2;

	@Column(name = "Option_3")
	String option3;

	@Column(name = "Option_4")
	String option4;

	@Column(name = "Option_5")
	String option5;

	@Column(name = "Correct_Answer")
	String correctAnswer;

	@Column(name = "Option_Type")
	String optionType;

	@Column(name = "QNo", insertable = false, updatable = false)
	int QNo;

	@ManyToOne
	@JoinColumn(name = "QNo", referencedColumnName = "QNo")
	Question question;

	public int getAnswerId() {
		return answerId;
	}

	public void setAnswerId(int answerId) {
		this.answerId = answerId;
	}

	public String getOption1() {
		return option1;
	}

	public void setOption1(String option1) {
		this.option1 = option1;
	}

	public String getOption2() {
		return option2;
	}

	public void setOption2(String option2) {
		this.option2 = option2;
	}

	public String getOption3() {
		return option3;
	}

	public void setOption3(String option3) {
		this.option3 = option3;
	}

	public String getOption4() {
		return option4;
	}

	public void setOption4(String option4) {
		this.option4 = option4;
	}

	public String getOption5() {
		return option5;
	}

	public void setOption5(String option5) {
		this.option5 = option5;
	}

	public String getCorrectAnswer() {
		return correctAnswer;
	}

	public void setCorrectAnswer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public String getOptionType() {
		return optionType;
	}

	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}

	public int getQNo() {
		return QNo;
	}

	public void setQNo(int qNo) {
		QNo = qNo;
	}

}
