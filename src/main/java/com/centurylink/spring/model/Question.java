package com.centurylink.spring.model;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "question")
@NamedQueries(value = {
		@NamedQuery(name = "Question.findByType", query = "SELECT q from Question q where q.questionType=:question_type"),
		@NamedQuery(name = "Question.findAll", query = "SELECT q from Question q") })
public class Question implements Serializable {

	/*
	 * QNo int(11) NOT NULL, Question_Desc varchar(2000) DEFAULT NULL,
	 * Question_Complexity varchar(15) DEFAULT NULL, Question_Type varchar(10)
	 * DEFAULT NULL, Question_Tech varchar(10) DEFAULT NULL, Question_Active
	 * varchar(2) DEFAULT NULL, Question_Time int(11) DEFAULT NULL,
	 */

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "QNo")
	int questionNo;

	@Column(name = "Question_Desc")
	String questionDesc;

	@Column(name = "Question_Complexity")
	String questionComplexity;

	@Column(name = "Question_Type")
	String questionType;

	@Column(name = "Question_Tech")
	String questionTechnology;

	@Column(name = "Question_Active")
	String questionActive;

	@Column(name = "Question_Time")
	int questionTime;

	@Column(name = "File_Name")
	String fileName;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "question")
	Collection<Applicability> applicability;

	public Collection<Applicability> getApplicability() {
		return applicability;
	}

	public void setApplicability(Collection<Applicability> applicability) {
		this.applicability = applicability;
	}

	@Column(name = "Template_code")
	String template_code;

	@Column(name = "Example")
	String example;

	public int getQuestionNo() {
		return questionNo;
	}

	public void setQuestionNo(int questionNo) {
		this.questionNo = questionNo;
	}

	public String getQuestionDesc() {
		return questionDesc;
	}

	public void setQuestionDesc(String questionDesc) {
		this.questionDesc = questionDesc;
	}

	public String getQuestionComplexity() {
		return questionComplexity;
	}

	public void setQuestionComplexity(String questionComplexity) {
		this.questionComplexity = questionComplexity;
	}

	public String getQuestionType() {
		return questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	public String getQuestionTechnology() {
		return questionTechnology;
	}

	public void setQuestionTechnology(String questionTechnology) {
		this.questionTechnology = questionTechnology;
	}

	public String getQuestionActive() {
		return questionActive;
	}

	public void setQuestionActive(String questionActive) {
		this.questionActive = questionActive;
	}

	public int getQuestionTime() {
		return questionTime;
	}

	public void setQuestionTime(int questionTime) {
		this.questionTime = questionTime;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getTemplate_code() {
		return template_code;
	}

	public void setTemplate_code(String template_code) {
		this.template_code = template_code;
	}

	public String getExample() {
		return example;
	}

	public void setExample(String example) {
		this.example = example;
	}

}
