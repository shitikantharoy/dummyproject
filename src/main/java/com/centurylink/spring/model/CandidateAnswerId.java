package com.centurylink.spring.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CandidateAnswerId implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Column(name="candidate_id")
	private int candidateId;
	
	@Column(name="qno")
	private int questionNo;
	
	public CandidateAnswerId (int candidateId, int questionNo) {
		this.candidateId = candidateId;
		this.questionNo = questionNo;
	}

	public int getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(int candidateId) {
		this.candidateId = candidateId;
	}

	public int getQuestionNo() {
		return questionNo;
	}

	public void setQuestionNo(int questionNo) {
		this.questionNo = questionNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + candidateId;
		result = prime * result + questionNo;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CandidateAnswerId other = (CandidateAnswerId) obj;
		if (candidateId != other.candidateId)
			return false;
		if (questionNo != other.questionNo)
			return false;
		return true;
	}

	
	
}
