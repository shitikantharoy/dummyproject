package com.centurylink.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "role")
public class Role {

	/*
	 * Role_ID int(11) NOT NULL, Role varchar(10) DEFAULT NULL, Description
	 * varchar(45) DEFAULT NULL, PRIMARY KEY (Role_ID)
	 */

	@Id
	@Column(name = "Role_ID")
	private int roleId;

	@Column(name = "Role")
	private String role;

	@Column(name = "Description")
	private String description;

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	

}
