package com.centurylink.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "testcase")
public class TestCase {

	@Id
	@Column(name = "Test_Case_id")
	private int testCaseId;

	@Column(name = "Test_Case")
	private String testCase;

	@Column(name = "Test_File_Name")
	private String testFileName;

	@Column(name = "Test_Case_Weightage")
	private int testCaseWeightage;

	@Column(name = "QNo")
	private int questionNo;

	@Column(name = "Test_Code")
	private String testCode;

	public int getTestCaseId() {
		return testCaseId;
	}

	public void setTestCaseId(int testCaseId) {
		this.testCaseId = testCaseId;
	}

	public String getTestCase() {
		return testCase;
	}

	public void setTestCase(String testCase) {
		this.testCase = testCase;
	}

	public String getTestFileName() {
		return testFileName;
	}

	public void setTestFileName(String testFileName) {
		this.testFileName = testFileName;
	}

	public int getTestCaseWeightage() {
		return testCaseWeightage;
	}

	public void setTestCaseWeightage(int testCaseWeightage) {
		this.testCaseWeightage = testCaseWeightage;
	}

	public int getQuestionNo() {
		return questionNo;
	}

	public void setQuestionNo(int questionNo) {
		this.questionNo = questionNo;
	}

	public String getTestCode() {
		return testCode;
	}

	public void setTestCode(String testCode) {
		this.testCode = testCode;
	}

}
