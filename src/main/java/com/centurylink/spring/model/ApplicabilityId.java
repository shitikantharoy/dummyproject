package com.centurylink.spring.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ApplicabilityId implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "Role_ID")
	private int roleId;

	public ApplicabilityId(int roleId) {
		super();
		this.roleId = roleId;
	}
	
	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + roleId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApplicabilityId other = (ApplicabilityId) obj;
		if (roleId != other.roleId)
			return false;
		return true;
	}

}
